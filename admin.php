<?php
    header("Content-Type: text/html; charset=utf-8");
    session_start();
    if (!empty($_SESSION['group'])) {
        if ($_SESSION['group'] != 'admins' and $_SESSION['group'] != 'global') {
            exit ("<html><head><meta http-equiv='Refresh' content='0; URL=index.php'></head></html>");
        }
    } else {
        exit ("<html><head><meta http-equiv='Refresh' content='0; URL=index.php'></head></html>");
    }
?>
<html>
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="images/games.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Marmelad|Neucha" rel="stylesheet">
    <script src='js/jquery-min.js'></script>
    <script src="js/jquery.json.min.js"></script>
    <title>Касса ArmGames</title>
</head>

<body>
    <header>
        <div class='info'>
            <div id="draws-button">
                <span>История Кено*22</span>
            </div>
            <div id="cashs-button">
                <span>Инкасация</span>
            </div>
            <div id="poker-button">
                <span>Раздачи Покера</span>
            </div>
        </div>
        <div class="i-frame-draws">
            <iframe class="draws" src="loading.html"></iframe>
            <button id="close-frame-draws"></button>
        </div>
        <div class="i-frame-cashs">
            <iframe class="cashs" src="loading.html"></iframe>
            <button id="close-frame-cashs"></button>
        </div>
        <div class="i-frame-poker">
            <iframe class="poker" src="loading.html"></iframe>
            <button id="close-frame-poker"></button>
        </div>
        <div class='welcome'>
            <span>
                Касса<br>"ArmGames"
            </span>
        </div>
        <div class='balance'>
            <span>
                <?php
                    include ("scripts/db.php");
                    $admin = $_SESSION['login'];
                    $res = $db->query("SELECT id, balance FROM admins WHERE login='$admin'");
                    $myadmin = $res->fetch_assoc();
                    $_SESSION['balance'] = $myadmin['balance'];
                    $_SESSION['id'] = $myadmin['id'];
                    echo "<b>Касса $admin</b><br>Баланс: <span id='money'>".$_SESSION['balance']."</span> драм";
                ?>
            </span>
            <br><a href='scripts/exit.php' class='link-button'>Выйти</a>
        </div>
    </header>
    <main class="admin">
        <div id='users'>
            <?php
                include ("scripts/db.php");
                $admin_id = $_SESSION['id'];
                $result = $db->query("(SELECT login, balance FROM users WHERE users.admin_id = '$admin_id') UNION (SELECT login, balance FROM admins WHERE admins.admin_id='$admin_id')");
                $users = $result->fetch_all(MYSQLI_ASSOC);
                for ($i=0, $users_length=count($users); $i<$users_length; $i++) {
                    $key = $users[$i];
                    echo "<div class='user-blocks'><span class='text name'>";
                    print_r($key['login']);
                    echo "</span><span class='text balance'>";
                    print_r($key['balance']);
                    echo "</span><span><input type='number' min='500' step='50' value='0' class='depozit' size=6></span><span><button class='plus'>+</button></span><span><button class='minus'>-</button></span></div>";
                };
            ?>
        </div>
    </main>
    <script src='js/admin.js'></script>
</body>

</html>