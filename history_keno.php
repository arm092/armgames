<!DOCTYPE html>

<?php
    header("Content-Type: text/html; charset=utf-8");
    session_start();
    if (empty($_SESSION['id'])) {
        exit("<html><head><meta http-equiv='Refresh' content='0; URL=index.php'></head></html>");
    };
?>
<html>

<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="images/games.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Marmelad|Neucha" rel="stylesheet">
    <script src='js/jquery-min.js'></script>
    <script src="js/jquery.json.min.js"></script>
    <title>История тиражей Кено*22</title>
</head>

<body>
    <header>
        <div class='welcome draw'>
            <span>
                История тиражей<br>"КЕНО*22"
            </span>
        </div>
    </header>
    <div class="main draw" >
        <div id="draws">
            <div class='draw-block'>
                <span class='text draw-id'>Тираж</span>
                <span class='text draw-numbers'>Выйгрышные номера</span>
                <span class='text draw-id'>Имя игрока</span>
                <span class='text draw-bet'>Ваши ставки</span>
                <span class='text draw-winings'>Сумма ставки</span>
                <span class='text draw-winings'>Ваш выйгрыш</span>
            </div>
            <?php
                include ("scripts/db.php");
                $login = $_SESSION['login'];
                if ($_SESSION['group'] == 'users') {
                    $result = $db->query("SELECT * FROM draws WHERE login='$login' OR login='-----' ORDER BY id DESC LIMIT 15");
                } else {
                    $result = $db->query("SELECT * FROM draws ORDER BY id DESC LIMIT 30");
                }
                $draws = $result->fetch_all(MYSQLI_ASSOC);
                for ($i=0, $draws_length=count($draws); $i<$draws_length; $i++) {
                    $key = $draws[$i];
                    echo "<div class='draw-block'><span class='text draw-id'>";
                    print_r($key['id']);
                    echo "</span><span class='text draw-numbers'>";
                    print_r($key['numbers']);
                    echo "</span><span class='text draw-login'>";
                    print_r($key['login']);
                    echo "</span><span class='text draw-bet'>";
                    print_r($key['bet']);
                    echo "</span><span class='text draw-winings'>";
                    print_r($key['bet_sum']);
                    echo "</span><span class='text draw-login'>";
                    print_r($key['winings']);
                    echo "</span></div>";
                };
            ?>
        </div>
    </div>
</body>
</html>