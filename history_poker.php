<!DOCTYPE html>

<?php
session_start();
if (empty($_SESSION['id'])) {
    exit("<html><head><meta http-equiv='Refresh' content='0; URL=index.php'></head></html>");
}
;
?>
<html>

<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Marmelad|Neucha" rel="stylesheet">
    <title>История раздач ПОКЕРА</title>
</head>

<body>

    <header>
        <div class='welcome draw'>
            <span>
                История раздач<br>ПОКЕРА
            </span>
        </div>
    </header>
    <div class="main draw" >
        <div id="draws">
            <div class='draw-block'>
                <span class='text draw-id'>Раздача</span>
                <span class='text draw-id'>Стол</span>
                <span class='text draw-numbers'>Карты стола</span>
                <span class='text draw-bet'>Карты игроков</span>
                <span class='text draw-winings'>Выйгрыш</span>
                <span class='text draw-winings'>Комбинация</span>
                <span class='text draw-winings'>Имя победителя</span>
            </div>
            <?php
include "scripts/db.php";
$result = $db->query("SELECT * FROM poker_history ORDER BY id DESC LIMIT 30");
$draws = $result->fetch_all(MYSQLI_ASSOC);
for ($i = 0, $draws_length = count($draws); $i < $draws_length; $i++) {
    $key = $draws[$i];
    echo "<div class='draw-block'><span class='text draw-id'>";
    print_r($key['id']);
    echo "</span><span class='text draw-id'>";
    print_r($key['table_name']);
    echo "</span><span class='text draw-numbers'>";
    print_r($key['table_cards']);
    echo "</span><span class='text draw-bet'>";
    print_r($key['players_cards']);
    echo "</span><span class='text draw-winings'>";
    print_r($key['win_sum']);
    echo "</span><span class='text draw-winings'>";
    print_r($key['win_combination']);
    echo "</span><span class='text draw-login'>";
    print_r($key['winer_login']);
    echo "</span></div>";
}
?>
        </div>
    </div>
</body>
</html>