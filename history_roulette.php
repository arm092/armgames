<!DOCTYPE html>

<?php
    header("Content-Type: text/html; charset=utf-8");
    session_start();
    if (empty($_SESSION['id'])) {
        exit("<html><head><meta http-equiv='Refresh' content='0; URL=index.php'></head></html>");
    };
?>
<html>

<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="images/games.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Marmelad|Neucha" rel="stylesheet">
    <script src='js/jquery-min.js'></script>
    <script src="js/jquery.json.min.js"></script>
    <title>История тиражей Рулетки</title>
</head>

<body>
    <header>
        <div class='welcome draw'>
            <span>
                История тиражей<br>РУЛЕТКИ
            </span>
        </div>
    </header>
    <div class="main draw" >
        <div id="draws">
            <div class='draw-block'>
                <span class='text draw-id'>Тираж</span>
                <span class='text draw-numbers'>Выйгрышный номер</span>
                <span class='text draw-id'>Цвет</span>
            </div>
            <?php
                include ("scripts/db.php");
                $login = $_SESSION['login'];
                if ($_SESSION['group'] == 'users') {
                    $result = $db->query("SELECT * FROM roulette_draws ORDER BY id DESC LIMIT 15");
                } else {
                    $result = $db->query("SELECT * FROM roulette_draws ORDER BY id DESC LIMIT 30");
                }
                $draws = $result->fetch_all(MYSQLI_ASSOC);
                for ($i=0, $draws_length=count($draws); $i<$draws_length; $i++) {
                    $key = $draws[$i];
                    echo "<div class='draw-block'><span class='text draw-id'>";
                    print_r($key['id']);
                    echo "</span><span class='text draw-numbers'>";
                    print_r($key['win_number']);
                    echo "</span><span class='text draw-login'>";
                    print_r($key['color']);
                    echo "</span></div>";
                };
            ?>
        </div>
    </div>
</body>
</html>