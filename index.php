<!DOCTYPE html>

<?php
    session_start();
    if (!empty($_SESSION['group'])){
        if ($_SESSION['group'] == 'admins' or $_SESSION['group'] == 'global') {
            echo ("<html><head><meta http-equiv='Refresh' content='0; URL=admin.php'></head></html>");
        };
    };
?>
<html>

<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="images/games.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Marmelad|Neucha" rel="stylesheet">
    <script src='js/jquery-min.js'></script>
    <script src="js/jquery.json.min.js"></script>
    <title>ArmGames</title>
</head>

<body>
    <section class="need-rotate">
        <div>
            <span>Переверните девайс, чтобы играть.</span>
            <img src='images/rotate.png'>
        </div>
    </section>
    <header>
        <div class='welcome'>
            <span>
                Добро пожаловать на<br>"ArmGames"
            </span>
        </div>
        <div class='user-block'>
                <?php
                    if (empty($_SESSION['login'])) {
                        echo "<b>Ваш игровой счет</b><br>Баланс: <span id='money'>0</span> драм";
                    }
                    else {
                        include ("scripts/db.php");
                        $user = $_SESSION['login'];
                        $res = $db->query("SELECT * FROM users WHERE login='$user'");
                        $mydata = $res->fetch_assoc();
                        $_SESSION['balance'] = $mydata['balance'];
                        $avatar_id = $mydata['avatar_id'];
                        $avatar_res = $db->query("SELECT img FROM avatars WHERE id='$avatar_id'");
                        $avatar = $avatar_res->fetch_assoc();
                        $_SESSION['avatar'] = $avatar['img'];
                        echo "<div class='login-info'><div><img src='".$_SESSION['avatar']."'></div><div><b id='user-login'>".$_SESSION['login']."</b><br>Баланс: <span id='money'>".$_SESSION['balance']."</span> драм</div><div><a href='scripts/exit.php' class='link-button' id='exit'>Выйти</a></div></div>";
                    }
                ?>
        </div>
    </header>
    <main>
        <div id='user-name-block'>
            <?php
                if (empty($_SESSION['login']) or empty($_SESSION['id'])) {
                    echo "<form action='scripts/test_user.php' method='post'><label for='login' class='text'>Имя пользователя:</label><br><input type='text' size='15' max-length='15' id='user-name' name='login'><br><label for='password' class='text'>Пароль:</label><br><input type='password' size='15' max-length='15' id='user-name' name='password'><br><br><input type='submit' style='cursor: pointer' name='submit' value='Войти'> <a href='reg.php' class='link-button'>Регистрация</a></form><br>Вы вошли на сайт, как гость<br><a href='#' style='color: white; font-size: 0.7em'>Эта ссылка  доступна только зарегистрированным пользователям</a>";
                } else {
                    echo "Вы вошли на сайт, как ".$_SESSION['login']."<br><button id='play'>Играть</button>";
                }
            ?>
        </div>
        <div class="choose">
            <div class="text">Выберете игру</div>
            <br>
            <a href="tic_tac_toe" title="Крестики-нолики"></a>
            <a href="keno" title="Кено*22"></a>
            <a href="poker" title="Покер Холдем"></a>
            <a href="roulette" title="Рулетка"></a>
        </div>
    </main>
    <script src="js/index.js"></script>
</body>
</html>