var rules;
var rulesImg;
var bank;
var bankCount;
var userBlock;
var userBlocks = [];
var plus = [];
var minus = [];
var depozit = [];
var userBalances = [];
var userName;
var userNames = [];
var users;

function ShowDraws() {
    $('.i-frame-draws').show(10, function() {
        $(this).off('click', ShowDraws);
        setTimeout(function() {
            $('iframe.draws').attr('src', 'history_keno.php');
        }, 1500);
        $('#close-frame-draws').click(function() {
            $('.i-frame-draws').hide(50, function() {
                $('iframe.draws').attr('src', 'loading.html');
                $(this).on('click', ShowDraws);
            });
        });
    });
}

function ShowCashs() {
    $('.i-frame-cashs').show(10, function() {
        $(this).off('click', ShowCashs);
        setTimeout(function() {
            $('iframe.cashs').attr('src', 'encashment.php');
        }, 1500);
        $('#close-frame-cashs').click(function() {
            $('.i-frame-cashs').hide(50, function() {
                $('iframe.cashs').attr('src', 'loading.html');
                $(this).on('click', ShowCashs);
            });
        });
    });
}

function ShowPoker() {
    $('.i-frame-poker').show(10, function() {
        $(this).off('click', ShowPoker);
        setTimeout(function() {
            $('iframe.poker').attr('src', 'history_poker.php');
        }, 1500);
        $('#close-frame-poker').click(function() {
            $('.i-frame-poker').hide(50, function() {
                $('iframe.poker').attr('src', 'loading.html');
                $(this).on('click', ShowPoker);
            });
        });
    });
}

function Plus() {
    this.classList.toggle('plus');
    let userID = this.className;
    let userPlus = 0;
    for (let i = 0; i < depozit.length; i++) {
        if (depozit[i].classList.contains(userID)) {
            userPlus = +depozit[i].value;
            depozit[i].value = 0;
            userBalance = +userBalances[i + 1].textContent;
            userBalance += userPlus;
        }
    }
    if (userPlus > 0) {
        $.getJSON('scripts/balance-users.php', { 'login': userID, 'sum': userPlus }, function(res) {
            userBalance = +res.balance;
            bank = +res.bank;
            for (let i = 0; i < depozit.length; i++) {
                if (depozit[i].classList.contains(userID)) {
                    userBalances[i + 1].innerHTML = userBalance;
                    bankCount.innerHTML = bank;
                }
            }
        });
    } else {
        alert('Введены неверные данные');
    }
    this.classList.toggle('plus');
}

function Minus() {
    this.classList.toggle('minus');
    let userID = this.className;
    let userMinus = 0;
    for (let i = 0; i < depozit.length; i++) {
        if (depozit[i].classList.contains(userID)) {
            userMinus = +depozit[i].value;
            depozit[i].value = 0;
            userBalance = +userBalances[i + 1].textContent;
        }
    }

    if (userMinus > 0) {
        $.getJSON('scripts/balance-users.php', { 'login': userID, 'sum': -userMinus }, function(res) {
            userBalance = +res.balance;
            if (userBalance >= 0) {
                bank = +res.bank;
                for (let i = 0; i < depozit.length; i++) {
                    if (depozit[i].classList.contains(userID)) {
                        userBalances[i + 1].innerHTML = userBalance;
                        bankCount.innerHTML = bank;
                    }
                }
            } else {
                alert('Недостаточно средств!');
                window.location.reload();
            }
        });
    } else {
        alert('Введены неверные данные');
    }
    this.classList.toggle('minus');
}

function Start() {
    users = document.querySelector('#users');
    bankCount = document.querySelector('#money');
    userBlock = document.querySelector('.user-blocks');
    $('#draws-button span').on('click', ShowDraws);
    $('#cashs-button span').on('click', ShowCashs);
    $('#poker-button span').on('click', ShowPoker);
    userBalances = document.querySelectorAll('.balance')
    userNames = document.querySelectorAll('.name');
    plus = document.querySelectorAll('.plus');
    minus = document.querySelectorAll('.minus');
    depozit = document.querySelectorAll('.depozit');
    bank = +bankCount.textContent;
    for (let i = 0; i < plus.length; i++) {
        console.log(userNames[i].textContent);
        userName = userNames[i].textContent;
        plus[i].addEventListener('click', Plus);
        plus[i].classList.add(userName);
        minus[i].addEventListener('click', Minus);
        minus[i].classList.add(userName);
        depozit[i].classList.add(userName);
    }
}

document.addEventListener("DOMContentLoaded", Start);