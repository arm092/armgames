var playButton = $('#play');
var chooseBlock = $('.choose');
if (playButton) {
    playButton.on('click', showGames);
}

function showGames() {
    $.getJSON('scripts/get.php', { 'login': $('#user-login').text() }, function(res) {
        balance = +res.result;
        $('#money').text(balance);
    });
    $('#user-name-block').hide();
    chooseBlock.show();
}