<!DOCTYPE html>

<?php
header("Content-Type: text/html; charset=utf-8");    
    session_start();
    if (!empty($_SESSION['group'])){
        if ($_SESSION['group'] == 'admins' or $_SESSION['group'] == 'global') {
            echo ("<html><head><meta http-equiv='Refresh' content='0; URL=../admin.php'></head></html>");
        };
    };
?>
<html>

<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Marmelad|Neucha" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <script src='../js/jquery-min.js'></script>
    <script src="../js/jquery.json.min.js"></script>
    <script src='../js/jquery.transform2d.js'></script>
    <title>Играть в КЕНО*22</title>
</head>

<body>
    <section class="need-rotate">
        <div>
            <span>Переверните девайс, чтобы играть.</span>
            <img src='../images/rotate.png'>
        </div>
    </section>
    <header>
        <div class='info'>
            <div>
                <span class='bold rules-button'>Коэффициенты</span>
                <img src="images/keno-rules.jpg" id="rules">
            </div>
            <div id='colors'>
                <b id='colors-button'>Информация</b>
                <span>Черный: Не выбранные
                    <br>Красный: Выбранные
                    <br>Желтый: Выигрышные
                    <br>Зеленый: Совпавшие
                </span>
            </div>
            <div id="draws-button">
                <span>История</span>
            </div>
        </div>
        <div class="i-frame-draws">
            <iframe class="draws" src="../loading.html"></iframe>
            <button id="close-frame-draws"></button>
        </div>
        <div class='welcome'>
            <span>
                Игра<br>"КЕНО*22"
            </span>
        </div>
        <div class='balance'>
            <span>
                <?php
                    if (empty($_SESSION['login'])) {
                        echo "<b>Ваш игровой счет</b><br>Баланс: <span id='money'>0</span> драм";
                    }
                    else {
                        include "../scripts/db.php";
                        $user = $_SESSION['login'];
                        $res = $db->query("SELECT * FROM users WHERE login='$user'");
                        $mydata = $res->fetch_assoc();
                        $_SESSION['balance'] = $mydata['balance'];
                        $avatar_id = $mydata['avatar_id'];
                        $avatar_res = $db->query("SELECT img FROM avatars WHERE id='$avatar_id'");
                        $avatar = $avatar_res->fetch_assoc();
                        $_SESSION['avatar'] = $avatar['img'];
                        echo "<div class='login-info'><div><img src='".$_SESSION['avatar']."'></div><div><b id='user-login'>".$_SESSION['login']."</b><br>Баланс: <span id='money'>".$_SESSION['balance']."</span> драм</div><div><a href='../scripts/exit.php' class='link-button' id='exit'>Выйти</a></div></div>";
                    }
                ?>
            </span>
        </div>
    </header>
    <div class="main">
        <div id='user-name-block'>
            <?php
                if (empty($_SESSION['login']) or empty($_SESSION['id'])) {
                    echo "<form action='../scripts/test_user.php' method='post'><label for='login' class='text'>Имя пользователя:</label><br><input type='text' size='15' max-length='15' id='user-name' name='login'><br><label for='password' class='text'>Пароль:</label><br><input type='password' size='15' max-length='15' id='user-name' name='password'><br><br><input type='submit' style='cursor: pointer' name='submit' value='Войти'> <a href='../reg.php' class='link-button'>Регистрация</a></form><br>Вы вошли на сайт, как гость<br><a href='#' style='color: white; font-size: 0.7em'>Эта ссылка  доступна только зарегистрированным пользователям</a>";
                }
                else {
                    echo "Вы вошли на сайт, как ".$_SESSION['login']."<br><button id='play'>Играть</button>";
                }
            ?>
        </div>
        <?php
            include ("../scripts/db.php");
            $res2_bank = $db->query("SELECT balance_active FROM admins WHERE id='1'");
            $mybank = $res2_bank->fetch_assoc();
            $data = $mybank['balance_active'];
            $_SESSION['bank'] = $data;
        ?>
        <div id='betting' >
            <div class='text'>Ваша ставка:</div>
            <input type='number' min='100' step='50' value='100' id='bet' size='6'>
            <span class='text'> драм</span><br><button id='make-bet'>Сделать ставку</button>
        </div>
        <div class='win-balls'>
            <table id='win-table'>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div id="game">
            <div id='table-box'>
                <span id='game-text'></span>
                <table id='game-field'>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                
                <button id='start'>Ставка</button>
                <button id='clean'>Очистить</button>
                <button id='clean-all'>Удалить</button>
                <button id='add'>Добавить</button>
            </div>
            <div id="timer">
                <div class="timing black"><span id="time"></span></div>
            </div>
            <div id='bet-box'>
                <span class='bet-text'></span>
                <table class='bet-box'>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <span class='bet-text'></span>
                <table class='bet-box'>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <span class='bet-text'></span>
                <table class='bet-box'>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <span class='bet-text'></span>
                <table class='bet-box'>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <div id="draw-num">
                    <span class="text">Тираж: </span><span class="text" id="draw-id">00000</span>
                </div>
            </div>
        </div>
    </div>
    <script src="js/main.js"></script>
</body>
</html>