<?php
include "../../scripts/db.php";
$draws_new = $db->query("SELECT * FROM draws_new ORDER BY id DESC LIMIT 20000");
$draws_new_id = $draws_new->fetch_assoc();
$last_id = $draws_new_id['id'] + 1;
$draw_id = $draws_new_id['id'];
$bal_bank = $db->query("SELECT balance_active FROM admins WHERE id = '1'");
$mybank = $bal_bank->fetch_assoc();
$bank = $mybank['balance_active'];
$bets = $db->query("SELECT * FROM bets WHERE draw_id='$last_id' ");
$bets_res = $bets->fetch_all(MYSQLI_ASSOC);
$bets_res_old = $db->query("SELECT * FROM bets WHERE draw_id='$draw_id' ");
$bets_old = $bets_res_old->fetch_all(MYSQLI_ASSOC);
$temp_array = [];
$bets_array = [];
$bets_old_array = [];
$mole_array = [];
$mole = [];
if ((!(empty($bets_res[0]['draw_id']))) && (!(empty($bets_old[0]['draw_id'])))) {
    for ($i = 0; $i < count($bets_res); $i++) {
        $bets_res[$i]['bet'] = explode(",", $bets_res[$i]['bet']);
        $temp_array[] = $bets_res[$i]['bet'];
    }
    $bets_array = call_user_func_array('array_merge', $temp_array);
    $temp_array = [];
    for ($i = 0; $i < count($bets_old); $i++) {
        $bets_old[$i]['bet'] = explode(",", $bets_old[$i]['bet']);
        $temp_array[] = $bets_old[$i]['bet'];
    }
    $bets_old_array = call_user_func_array('array_merge', $temp_array);
    $mole_array = array_diff($bets_old_array, $bets_array);
} elseif (empty($bets_res[0]['draw_id']) && (!(empty($bets_old[0]['draw_id'])))) {
    for ($i = 0; $i < count($bets_old); $i++) {
        $bets_old[$i]['bet'] = explode(",", $bets_old[$i]['bet']);
        $temp_array[] = $bets_old[$i]['bet'];
    }
    $mole_array = call_user_func_array('array_merge', $temp_array);
} elseif (empty($bets_old[0]['draw_id'])) {
    for ($i = 0; $i < count($bets_res); $i++) {
        $bets_res[$i]['bet'] = explode(",", $bets_res[$i]['bet']);
    }
}
if (count($mole_array) > 0) {
    shuffle($mole_array);
    $count = count($mole_array) / 2;
    while ($count > 22) {
        $count /= 2;
    }
    $mole = array_slice($mole_array, 0, $count);
    $mole_array = array_unique($mole, SORT_NUMERIC);
    $mole = $mole_array;
}

$coin = [];
$kayf = [];
$win_sum = [];
$winings = 0;
$bet_sum = 0;
if (!(empty($bets_res[0]['draw_id']))) {
    for ($i = 0; $i < count($bets_res); $i++) {
        $coin[$i] = 0;
        $kayf[$i] = 0;
        $win_sum[$i] = 0;
        $bet_sum += $bets_res[$i]['bet_sum'];
    }

    $user_games = $db->query("SELECT * FROM draws_new ORDER BY id DESC LIMIT 20000");
    $user_win_games = $db->query("SELECT * FROM draws_new WHERE win_sum > bet_sum ");
    $res_games = $user_games->fetch_all(MYSQLI_ASSOC);
    $games = count($res_games);
    $res_win_games = $user_win_games->fetch_all(MYSQLI_ASSOC);
    $win_games = count($res_win_games);
    $y = 0;
    do {
        do {
            if (count($mole) > 0) {
                $win_numbers = $mole;
            } else {
                $win_numbers = [];
            }
            while (count($win_numbers) < 22) {
                $y = rand(1, 80);
                if (!(in_array($y, $win_numbers))) {
                    $win_numbers[] = $y;
                }
            }
            ;
            for ($j = 0; $j < count($bets_res); $j++) {
                for ($i = 0; $i < count($win_numbers); $i++) {
                    if (in_array($win_numbers[$i], $bets_res[$j]['bet'])) {
                        $coin[$j]++;
                    }
                }
            }
            for ($i = 0; $i < count($bets_res); $i++) {
                switch ($coin[$i]) {
                    case 0:
                        switch (count($bets_res[$i]['bet'])) {
                            case 7:
                                $kayf[$i] = 1;
                                break;
                            case 8:
                                $kayf[$i] = 1;
                                break;
                            case 9:
                                $kayf[$i] = 2;
                                break;
                            case 10:
                                $kayf[$i] = 2;
                                break;
                            default:
                                $kayf[$i] = 0;
                                break;
                        }
                        break;
                    case 1:
                        switch (count($bets_res[$i]['bet'])) {
                            case 1:
                                $kayf[$i] = 3;
                                break;
                            case 2:
                                $kayf[$i] = 1;
                                break;
                            default:
                                $kayf[$i] = 0;
                                break;
                        }
                        break;
                    case 2:
                        switch (count($bets_res[$i]['bet'])) {
                            case 2:
                                $kayf[$i] = 10;
                                break;
                            case 3:
                                $kayf[$i] = 2;
                                break;
                            case 4:
                                $kayf[$i] = 1;
                                break;
                            case 5:
                                $kayf[$i] = 1;
                                break;
                            default:
                                $kayf[$i] = 0;
                                break;
                        }
                        break;
                    case 3:
                        switch (count($bets_res[$i]['bet'])) {
                            case 3:
                                $kayf[$i] = 45;
                                break;
                            case 4:
                                $kayf[$i] = 10;
                                break;
                            case 5:
                                $kayf[$i] = 3;
                                break;
                            case 6:
                                $kayf[$i] = 2;
                                break;
                            case 7:
                                $kayf[$i] = 2;
                                break;
                            default:
                                $kayf[$i] = 0;
                                break;
                        }
                        break;
                    case 4:
                        switch (count($bets_res[$i]['bet'])) {
                            case 4:
                                $kayf[$i] = 80;
                                break;
                            case 5:
                                $kayf[$i] = 20;
                                break;
                            case 6:
                                $kayf[$i] = 15;
                                break;
                            case 7:
                                $kayf[$i] = 4;
                                break;
                            case 8:
                                $kayf[$i] = 5;
                                break;
                            case 9:
                                $kayf[$i] = 2;
                                break;
                            default:
                                $kayf[$i] = 0;
                                break;
                        }
                        break;
                    case 5:
                        switch (count($bets_res[$i]['bet'])) {
                            case 5:
                                $kayf[$i] = 150;
                                break;
                            case 6:
                                $kayf[$i] = 60;
                                break;
                            case 7:
                                $kayf[$i] = 20;
                                break;
                            case 8:
                                $kayf[$i] = 15;
                                break;
                            case 9:
                                $kayf[$i] = 10;
                                break;
                            case 10:
                                $kayf[$i] = 5;
                                break;
                            default:
                                $kayf[$i] = 0;
                                break;
                        }
                        break;
                    case 6:
                        switch (count($bets_res[$i]['bet'])) {
                            case 6:
                                $kayf[$i] = 500;
                                break;
                            case 7:
                                $kayf[$i] = 80;
                                break;
                            case 8:
                                $kayf[$i] = 50;
                                break;
                            case 9:
                                $kayf[$i] = 25;
                                break;
                            case 10:
                                $kayf[$i] = 30;
                                break;
                            default:
                                $kayf[$i] = 0;
                                break;
                        }
                        break;
                    case 7:
                        switch (count($bets_res[$i]['bet'])) {
                            case 7:
                                $kayf[$i] = 1000;
                                break;
                            case 8:
                                $kayf[$i] = 200;
                                break;
                            case 9:
                                $kayf[$i] = 125;
                                break;
                            case 10:
                                $kayf[$i] = 100;
                                break;
                            default:
                                $kayf[$i] = 0;
                                break;
                        }
                        break;
                    case 8:
                        switch (count($bets_res[$i]['bet'])) {
                            case 8:
                                $kayf[$i] = 2000;
                                break;
                            case 9:
                                $kayf[$i] = 1000;
                                break;
                            case 10:
                                $kayf[$i] = 300;
                                break;
                            default:
                                $kayf[$i] = 0;
                                break;
                        }
                        break;
                    case 9:
                        switch (count($bets_res[$i]['bet'])) {
                            case 9:
                                $kayf[$i] = 5000;
                                break;
                            case 10:
                                $kayf[$i] = 2000;
                                break;
                            default:
                                $kayf[$i] = 0;
                                break;
                        }
                        break;
                    case 10:
                        switch (count($bets_res[$i]['bet'])) {
                            case 10:
                                $kayf[$i] = 10000;
                                break;
                            default:
                                $kayf[$i] = 0;
                                break;
                        }
                        break;
                    default:
                        $kayf[$i] = 0;
                        break;
                }
            }
            $winings = 0;
            for ($i = 0; $i < count($bets_res); $i++) {
                $win_sum[$i] += $bets_res[$i]['bet_sum'] * $kayf[$i];
                $win_sum[$i] = round($win_sum[$i]);
                $winings += $win_sum[$i];
            }
            ;
        } while ($winings > $bank);
        if ($win_games > 0.2 * $games) {
            $winings *= $bank;
        }
    } while ($winings > $bank);
    for ($i = 0; $i < count($bets_res); $i++) {
        $bet_id = $bets_res[$i]['id'];
        $bet_res = $db->query("UPDATE bets SET win_sum = '$win_sum[$i]', coin = '$coin[$i]' WHERE id = '$bet_id'");
    }
} else {
    if (count($mole) > 0) {
        $win_numbers = $mole;
    } else {
        $win_numbers = [];
    }
    do {
        do {
            $y = rand(1, 80);
        } while (in_array($y, $win_numbers));
        $win_numbers[] = $y;
    } while (count($win_numbers) < 22);
}
;
$win_str = '';
shuffle($win_numbers);
for ($i = 0, $count = count($win_numbers); $i < $count; $i++) {
    if ($i == $count - 1) {
        $win_str .= "$win_numbers[$i]";
    } else {
        $win_str .= "$win_numbers[$i], ";
    }
    ;
}
;
if (empty($bets_res[0]['draw_id'])) {
    $draw_res = $db->query("INSERT INTO draws (id, numbers, login, user_balance, bet, bet_sum, winings) VALUES ('$last_id', '$win_str', '-----', '0', '-----', '0', '0' )");
}
$time = time();
$win_res = $db->query("INSERT INTO draws_new (draw_time, numbers, bet_sum, win_sum) VALUES ('$time', '$win_str', '$bet_sum', '$winings')");
$bank -= $winings;
$res_bank = $db->query("UPDATE admins SET balance_active = '$bank' WHERE id = '1'");
