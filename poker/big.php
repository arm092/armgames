<!DOCTYPE html>

<?php
session_start();
if (empty($_SESSION['group'])) {
    echo ("<html><head><meta http-equiv='Refresh' content='0; URL=../index.php'></head></html>");
} else {
    if ($_SESSION['group'] == 'admins' or $_SESSION['group'] == 'global') {
        echo ("<html><head><meta http-equiv='Refresh' content='0; URL=../admin.php'></head></html>");
    }
}

?>
<html>
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="images/favico.ico" type="image/x-icon">    
    <link href="https://fonts.googleapis.com/css?family=Marmelad|Neucha" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <script src='../js/jquery-min.js'></script>
    <script src="../js/jquery.json.min.js"></script>
    <script src="../js/jquery.dataAttr.js"></script>
    <script src="../js/ion.sound.min.js"></script>
    <title>Table 100/200</title>
</head>
    <body>
        <section class="need-rotate">
            <div>
                <span>Переверните девайс, чтобы играть.</span>
                <img src='../images/rotate.png'>
            </div>
        </section>
        <section>
        <div id="poker-table">
        <table>
            <tr>
                <td class="balance"></td>
                <td class="croupier" colspan=4 rowspan=2>
                    <img src='images/croupier.png' class="croupier-img">
                </td>
                <td class="balance"></td>
            </tr>
            <tr>
                <td class="name"></td>
                <td class="name"></td>
            </tr>
            <tr>
                <td class="avatar">
                    <img class="avatar-img">
                </td>
                <td class="bet">
                    <img src='images/5000.png'>
                    <span></span>
                </td>
                <td class="bank">
                    <span>0</span>
                </td>
                <td class="bet-last">
                    <span>100</span>
                </td>
                <td class="bet">
                    <img src='images/5000.png'>
                    <span></span>
                </td>
                <td class="avatar">
                    <img class="avatar-img">
                </td>
            </tr>
            <tr>
                <td></td>
                <td class="coment"></td>
                <td class="coment-game" colspan=2></td>
                <td class="coment"></td>
                <td></td>
            </tr>
            <tr class="cards-row">
                <td class="cards" rowspan=2>
                    <img src="images/back.svg">
                    <img src="images/back.svg">
                </td>
                <td colspan=4 rowspan=4 class="cards-playing">
                    <span class='alert'>Дождитесь второго игрока...</span>
                    <span class='alert'>Дождитесь следующей раздачи...</span>
                    <img src="images/back.svg">
                    <img src="images/back.svg">
                    <img src="images/back.svg">
                    <img src="images/back.svg">
                    <img src="images/back.svg">
                </td>
                <td class="cards" rowspan=2>
                    <img src="images/back.svg">
                    <img src="images/back.svg">
                </td>
            </tr>
            <tr></tr>
            <tr class="cards-row">
                <td class="cards" rowspan=2>
                    <img src="images/back.svg">
                    <img src="images/back.svg">
                </td>
                <td class="cards" rowspan=2>
                    <img src="images/back.svg">
                    <img src="images/back.svg">
                </td>
            </tr>
            <tr></tr>
            <tr>
                <td></td>
                <td class="coment"></td>
                <td class="coment-player" colspan=2></td>
                <td class="coment"></td>
                <td></td>
            </tr>
            <tr class="cards-row">
                <td class="avatar">
                    <img class="avatar-img">
                </td>
                <td class="bet">
                    <img src='images/5000.png'>
                    <span></span>
                </td>
                <td class="cards-player">
                    <img src="images/back.svg">
                    <img src="images/back.svg">
                </td>
                <td class="bet-player">
                    <img src="images/5000.png">
                    <span></span>
                </td>
                <td class="bet">
                    <img src='images/5000.png'>
                    <span></span>
                </td>
                <td class="avatar">
                    <img class="avatar-img">
                </td>
            </tr>
            <tr>
                <td class="name"></td>
                <td class="in-button">
                    <button id="rebuy">Rebuy</button>
                </td>
                <td class="avatar-player" colspan=2 rowspan=2>
                    <img class="avatar-player-img" src=<?php echo "'" . $_SESSION['avatar'] . "'" ?>>
                </td>
                <td class="in-button">
                    <button id="check">Check</button>
                </td>
                <td class="name"></td>
            </tr>
            <tr>
                <td class="balance"></td>
                <td class="in-button">
                    <button id="exit-table">Exit</button>
                </td>
                <td class="in-button">
                    <button id="fold">Fold</button>
                </td>
                <td class="balance"></td>
            </tr>
            <tr>
                <td colspan=2 class="in-button">
                    <input type="text" name="coment" id="coment">
                </td>
                <td class="name-player" colspan=2 data-color=<?php echo '"' . $_SESSION['color'] . '">';
echo $_SESSION['login'] ?></td>
                <td class="in-button" colspan=2>
                    <input type="number" id="betings" value="200" step="100">
                    <button id="apply-bet">Bet</button>
                </td>
            </tr>
            <tr>
                <td colspan=2 class="in-button">
                    <button id="apply-coment">OK</button>
                </td>
                <td class="balance-player" id="player-balance" colspan=2 data-color=<?php echo '"' . $_SESSION['color'] . '">';
echo $_SESSION['balance_active'] ?></td>
                <td colspan=2 class="in-button">
                    <button id="table-refresh">Refresh</button>
                </td>
            </tr>
        </table>
        </div>
        <div class="rebuy">
            <div class="text">Фишки для игры</div>
            <input type="number" id="rebuy-sum" min="10000" step="200">
            <button id="rebuy-button">OK</button>
        </div>
        <div class="deck">
            <img src="images/back.svg">
            <img src="images/back.svg">
            <img src="images/back.svg">
            <img src="images/back.svg">
            <img src="images/back.svg">
            <img src="images/back.svg">
            <img src="images/back.svg">
            <img src="images/back.svg">
            <img src="images/back.svg">
            <img src="images/back.svg">
        </div>
        </section>
        <script src="js/big.js"></script>
    </body>
</html>
