<!DOCTYPE html>

<?php
    session_start();
    if (!empty($_SESSION['group'])){
        if ($_SESSION['group'] == 'admins' or $_SESSION['group'] == 'global') {
            echo ("<html><head><meta http-equiv='Refresh' content='0; URL=../admin.php'></head></html>");
        };
    };
?>
<html>

<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="images/favico.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Marmelad|Neucha" rel="stylesheet">
    <link rel="stylesheet" href="styles_main.css">
    <script src='../js/jquery-min.js'></script>
    <script src="../js/jquery.json.min.js"></script>
    <script src="../js/jquery.dataAttr.js"></script>
    <title>Играть в ПОКЕР</title>
</head>

<body>
    <section class="need-rotate">
        <div>
            <span>Переверните девайс, чтобы играть.</span>
            <img src='../images/rotate.png'>
        </div>
    </section>
    <header>
        <div class='info'>
        </div>
        <div class='welcome'>
            <span>
                Игра<br>"ПОКЕР ХОЛДЕМ"
            </span>
        </div>
        <div class='user-block'>
                <?php
                    if (empty($_SESSION['login'])) {
                        echo "<b>Ваш игровой счет</b><br>Баланс: <span id='money'>0</span> драм";
                    }
                    else {
                        include "../scripts/db.php";
                        $user = $_SESSION['login'];
                        $res = $db->query("SELECT * FROM users WHERE login='$user'");
                        $mydata = $res->fetch_assoc();
                        $_SESSION['balance'] = $mydata['balance'];
                        $avatar_id = $mydata['avatar_id'];
                        $avatar_res = $db->query("SELECT img FROM avatars WHERE id='$avatar_id'");
                        $avatar = $avatar_res->fetch_assoc();
                        $_SESSION['avatar'] = $avatar['img'];
                        echo "<div class='login-info'><div><img src='".$_SESSION['avatar']."'></div><div><b id='user-login'>".$_SESSION['login']."</b><br>Баланс: <span id='money'>".$_SESSION['balance']."</span> драм</div><div><a href='../scripts/exit.php' class='link-button' id='exit'>Выйти</a></div></div>";
                    }
                ?>
        </div>
    </header>
    <main>
        <div id='user-name-block'>
            <?php
                if (empty($_SESSION['login']) or empty($_SESSION['id'])) {
                    echo "<form action='../scripts/test_user.php' method='post'><label for='login' class='text'>Имя пользователя:</label><br><input type='text' size='15' max-length='15' id='user-name' name='login'><br><label for='password' class='text'>Пароль:</label><br><input type='password' size='15' max-length='15' id='user-name' name='password'><br><br><input type='submit' style='cursor: pointer' name='submit' value='Войти'> <a href='../reg.php' class='link-button'>Регистрация</a></form><br>Вы вошли на сайт, как гость<br><a href='#' style='color: white; font-size: 0.7em'>Эта ссылка  доступна только зарегистрированным пользователям</a>";
                } else {
                    echo "Вы вошли на сайт, как ".$_SESSION['login']."<br><button id='play'>Играть</button>";
                }
            ?>
        </div>
        <div class="choose">
            <div class="text">Выберете стол</div>
            <button data-url="small" data-step="50" data-max="10000" data-min="1000">Стол 25/50</button>
            <button data-url="normal" data-step="100" data-max="25000" data-min="5000">Стол 50/100</button>
            <button data-url="big" data-step="500" data-max="50000" data-min="10000">Стол 100/200</button>
            <button data-url="royal" data-step="1000" data-max="100000" data-min="20000">Стол 250/500</button>
        </div>
        <div class="rebuy">
            <div class="text" id="msg">Фишки для игры</div>
            <input type="number" id="rebuy-sum">
            <button id="rebuy-button">OK</button>
            <div class="limits">
                <span class="text">Минимум для стола: </span><span class="text" id="min-limit"></span>
                <br/>
                <span class="text">Максимум для стола: </span><span class="text" id="max-limit"></span>
            </div>
        </div>
        <div class="i-frame">
            <iframe src="loading.html"></iframe>
        </div>
    </main>
    <script src="js/index.js"></script>
</body>
</html>