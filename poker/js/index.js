var playButton = $('#play');
var tableButtons = document.querySelectorAll('.choose button');
var chooseBlock = $('.choose');
var rebuyBlock = $('.rebuy');
var rebuySum = $('.rebuy input');
var rebuyOk = $('.rebuy button');

function openTable() {
    let min = +this.dataset.min;
    let max = +this.dataset.max;
    $('#min-limit').text(min);
    $('#max-limit').text(max);
    $.getJSON('../scripts/get.php', {}, function(res) {
        balance = +res.result;
        $('#money').text(balance);
    });
    let balance = $('#money').text();
    chooseBlock.hide();
    rebuyBlock.show();
    rebuyOk.attr('data-url', this.dataset.url);
    if (min > balance) {
        $('.rebuy #msg').html('У вас недостаточно средств,<br>чтобы играть за этим столом.');
        rebuySum.hide()
        rebuyOk.text('Назад');
        rebuyOk.on('click', () => {
            $.getJSON('../scripts/get.php', {}, function(res) {
                balance = +res.result;
                $('#money').text(balance);
            });
            rebuySum.show();
            rebuyOk.text('OK');
            $('.rebuy #msg').text('Фишки для игры');
            rebuyBlock.hide();
            chooseBlock.show();
            rebuyOk.off();
        });
    } else {
        rebuySum.attr('min', min);
        rebuySum.val(min);
        if (max > balance) {
            max = balance;
        }
        rebuySum.attr('max', max);
        rebuySum.attr('step', this.dataset.step);
        rebuyOk.on('click', () => {
            $.getJSON('../scripts/get.php', {}, function(res) {
                balance = +res.result;
                $('#money').text(balance);
            });
            if (rebuySum.val() <= +balance) {
                $.getJSON('scripts/rebuy.php', { 'rebuy': rebuySum.val(), 'table': rebuyOk.attr('data-url') }, function(res) {
                    balance = +res.balance;
                    $('#money').text(balance);
                    if (res.msg == "OK") {
                        rebuyBlock.hide();
                        rebuyOk.off();
                        $('.i-frame').show();
                        setTimeout(function() {
                            window.location.replace(rebuyOk.attr('data-url') + '.php');
                        }, 1500);
                    } else {
                        $('.rebuy #msg').html(res.msg);
                        rebuySum.hide()
                        rebuyOk.text('Назад');
                        rebuyOk.on('click', () => {
                            $.getJSON('../scripts/get.php', {}, function(res) {
                                balance = +res.result;
                                $('#money').text(balance);
                                rebuySum.show();
                                rebuyOk.text('OK');
                                $('.rebuy #msg').text('Фишки для игры');
                                rebuyBlock.hide();
                                chooseBlock.show();
                                rebuyOk.off();
                            });
                        });
                    }
                });

            } else {
                alert('Недостаточно средств!');
                return;
            }
        });
    }
}

function showTables() {
    $.getJSON('../scripts/get.php', {}, function(res) {
        balance = +res.result;
        $('#money').text(balance);
    });
    $('#user-name-block').hide();
    chooseBlock.show();
}
for (let i = 0; i < tableButtons.length; i++) {
    $(tableButtons[i]).on('click', openTable);
};
if (playButton) {
    playButton.on('click', showTables);
}