var table = 'small';
var halfBet = 25;
var minBet = 50;
var limitMin = 1000;
var limitMax = 10000;
var commentArea = $('#comment');
var commentOk = $('#apply-comment');
var rebuyB = $('#rebuy');
var exitB = $('#exit-table');
var checkB = $('#check');
var foldB = $('#fold');
foldB.addClass('red');
var betings = $('#betings');
var betB = $('#apply-bet');
checkB.hide();
foldB.hide();
betings.hide();
betB.hide();
var balanceTd = $('#player-balance');
var refreshB = $('#table-refresh');
var rebuyBlock = $('.rebuy');
var rebuyOk = $('#rebuy-button');
var rebuySum = $('#rebuy-sum');
var lastBet = $('.bet-last span');
var bank = $('.bank span');
var namePlayer = $('.name-player');
lastBet.hide();
bank.hide();
commentOk.on('click', applycomment);
refreshB.on('click', refresh);
rebuyB.on('click', showRebuy);
rebuyB.attr('disabled', true);
rebuyOk.on('click', rebuy);
exitB.on('click', exit);
betB.on('click', bet);
checkB.on('click', check);
foldB.on('click', fold);
var timeOut;
var interval;
var clearcomments;
var gameStatus = ['preflop', 'flop', 'turn', 'river'];
var gs = 1;
var noBet = false;
var presenceVar = 0;

ion.sound({
    sounds: [
        { name: "bet_call" },
        { name: "change_status" },
        { name: "check" },
        { name: "deal" },
        { name: "fold" },
        { name: "win_sound" }
    ],

    // main config
    path: "sounds/",
    preload: true,
    multiplay: true,
    volume: 1
});

function presence(num) {
    if ((presenceVar == 0) && (num == 0)) {
        return;
    } else {
        $.getJSON('scripts/presence.php', { 'table': table, 'here': num }, function(res) {
            presenceVar = +res.here;
            if (presenceVar >= 5) {
                exit();
            }
        });
    }
}

function loadGame() {
    $.getJSON('scripts/game.php', { 'table': table, 'minbet': halfBet }, function(res) {
        balanceTd.css('background-color', res.player.color);
        namePlayer.css('background-color', res.player.color);
        if (res.players.length < 2) {
            $('.alert').eq(0).show();
        } else {
            $('.alert').eq(0).hide();
            for (let i = 1; i < res.players.length; i++) {
                $('table').find('.avatar-img').eq(i - 1).attr('src', res.players[i].avatar_url).css('opacity', '1').dataAttr('name', res.players[i].login);
                $('table').find('.balance').eq(i - 1).text(res.players[i].balance).dataAttr('name', res.players[i].login).css('background-color', res.players[i].color).css('opacity', '1');
                $('table').find('.name').eq(i - 1).text(res.players[i].login).dataAttr('name', res.players[i].login).css('background-color', res.players[i].color).css('opacity', '1');
                $('table').find('.comment').eq(i - 1).text(res.players[i].comment).dataAttr('name', res.players[i].login);
                switch (res.players[i].comment) {
                    case 'Check':
                        ion.sound.play('check');
                        break;
                    case 'Call':
                        ion.sound.play('bet_call');
                        break;
                    case 'Bet/Raise':
                        ion.sound.play('bet_call');
                        break;
                    case 'Fold':
                        ion.sound.play('fold');
                        break;
                    case 'All In':
                        ion.sound.play('bet_call');
                        break;
                    default:
                        break;
                }
                if (+res.players[i].bet > 0) {
                    $('table').find('.bet').eq(i - 1).find('span').text(res.players[i].bet).show();
                    $('table').find('.bet').eq(i - 1).dataAttr('name', res.players[i].login);
                    $('table').find('.bet').eq(i - 1).find('img').show();
                } else {
                    $('table .bet').find('span').text(0).hide();
                    $('table .bet').find('img').hide();
                }
                if (res.players[i].comment == '') {
                    $('table').find('.comment').eq(i - 1).css('opacity', '0');
                } else {
                    $('table').find('.comment').eq(i - 1).css('opacity', '1');
                }

                if (res.players[0].turn == 0) {
                    $('table').find('.cards').eq(i - 1).find('img').show();
                    $('table').find('.cards').eq(i - 1).dataAttr('name', res.players[i].login);
                    $('table').find('.cards-playing').find('img').show();
                }
                if (res.players[i].winner == 1) {
                    gs = -1;
                    clearInterval(interval);
                    clearInterval(clearcomments);
                    $('table').find('.cards[data-name=' + res.players[i].login + ']').find('img').eq(0).attr('src', res.players[i].card3).addClass('winner-cards');
                    $('table').find('.cards[data-name=' + res.players[i].login + ']').find('img').eq(1).attr('src', res.players[i].card4).addClass('winner-cards');
                    $('table').find('.balance[data-name=' + res.players[i].login + ']').addClass('winner');
                    $('table').find('.name[data-name=' + res.players[i].login + ']').addClass('winner');
                    $('table').find('img[data-name=' + res.players[i].login + ']').addClass('winner');
                    setTimeout(() => {
                        $('table').find('.cards[data-name=' + res.players[i].login + ']').find('img').attr('src', 'images/back.svg').removeClass('winner-cards');
                        $('table').find('.balance[data-name=' + res.players[i].login + ']').removeClass('winner');
                        $('table').find('.name[data-name=' + res.players[i].login + ']').removeClass('winner');
                        $('table').find('img[data-name=' + res.players[i].login + ']').removeClass('winner');
                    }, 10000);
                }
            }
            $('.comment-player').text(res.player.comment);
            if (res.player.comment == '') {
                $('.comment-player').css('opacity', '0');
            } else {
                $('.comment-player').css('opacity', '1');
            }
            balanceTd.text(res.player.balance);
            if (+res.player.balance < minBet) {
                rebuyB.removeAttr('disabled');
            } else {
                rebuyB.attr('disabled', true);
            }
            if (res.player.winner == 1) {
                gs = -1;
                ion.sound.play('win_sound');
                clearInterval(interval);
                clearInterval(clearcomments);
                balanceTd.addClass('winner');
                namePlayer.addClass('winner');
                $('.avatar-player-img').addClass('winner');
                $('.cards-player img').eq(0).addClass('winner-cards');
                $('.cards-player img').eq(1).addClass('winner-cards');
                setTimeout(() => {
                    addcomment('');
                    namePlayer.removeClass('winner');
                    balanceTd.text(res.player.balance).removeClass('winner');
                    $('.avatar-player-img').removeClass('winner');
                    $('.cards-player img').eq(0).removeClass('winner-cards');
                    $('.cards-player img').eq(1).removeClass('winner-cards');
                }, 10000);
            }
            if (res.players[0].bet >= 0) {
                bank.text(+res.players[0].bet).show();
            } else {
                bank.text(0).show();
            }
            if (res.player.in_play == 0) {
                $('.alert').eq(1).show();
            } else {
                $('.alert').hide();
            }
            if ((gs == -1) || (res.players[0].turn == 1)) {
                clearInterval(interval);
                $('.alert').hide();
                gs = 1;
                setTimeout(deal, 10000);
            } else {
                $('.cards-player img').eq(0).attr('src', res.card1).show();
                $('.cards-player img').eq(1).attr('src', res.card2).show();

                if (+res.players[0].in_play > gs) {
                    gs = +res.players[0].in_play;
                    changeGameStatus(gameStatus[gs - 1]);
                }
                if (+res.last_bet >= 0) {
                    lastBet.text(+res.last_bet).show();
                }
                if (+res.player.bet > 0) {
                    $('.bet-player img').show();
                    $('.bet-player span').text(+res.player.bet).show();
                } else {
                    $('.bet-player img').hide();
                    $('.bet-player span').hide();
                }
                if (+res.players[0].winner == (1 + res.players_inplay)) {
                    gs++;
                    if (gs < 5) {
                        changeGameStatus(gameStatus[gs - 1]);
                    } else {
                        endOfGame();
                    }
                } else {
                    if ((res.players[0].comment != null) && (isNaN(+res.players[0].comment))) {
                        $('.comment-game').css('opacity', '1').text('Очередь: ' + res.players[0].comment);
                    }
                    if (res.players[0].comment == namePlayer.text()) {
                        for (let i = 1; i < res.players.length; i++) {
                            if ((res.players[i].balance == 0) && (res.players[i].bet == 0) && (res.players[0].winner < res.players.length)) {
                                noBet = true;
                            }
                        }
                        if (noBet) {
                            check();
                        } else if (+balanceTd.text() > 0) {
                            betings.attr('min', 2 * +lastBet.text());
                            if (betings.val() < (2 * +lastBet.text())) {
                                betings.val(2 * +lastBet.text());
                            }
                            if (+lastBet.text() > 0) {
                                betB.text('Raise');
                                if (+betings.val() >= +balanceTd.text()) {
                                    betB.off().text('All In').addClass('green').on('click', allIn);
                                } else {
                                    betB.off().text('Raise').removeClass('green').on('click', bet);
                                }
                            } else {
                                betB.text('Bet');
                                betings.attr('min', minBet);
                                if (betings.val() < minBet) {
                                    betings.val(minBet);
                                }
                                if (+betings.val() >= +balanceTd.text()) {
                                    betB.off().text('All In').addClass('green').on('click', allIn);
                                } else {
                                    betB.off().text('Bet').removeClass('green').on('click', bet);
                                }
                            }
                            if (+lastBet.text() > +res.player.bet) {
                                checkB.addClass('green');
                                if (+lastBet.text() < +balanceTd.text()) {
                                    checkB.text("Call");
                                    checkB.off();
                                    checkB.on('click', call);
                                    betB.show();
                                    betings.show();
                                } else {
                                    checkB.text("All In");
                                    checkB.off();
                                    checkB.on('click', allIn);
                                }
                            } else {
                                checkB.off();
                                checkB.on('click', check);
                                checkB.text('Check').removeClass('green');
                                betB.show();
                                betings.show();
                            }
                            clearInterval(interval);
                            timeOut = setTimeout(() => {
                                fold();
                                presence(1);
                            }, 20000);
                            checkB.show();
                            foldB.show();
                        } else {
                            check();
                        }
                    }
                }
            }
        }

    });
}

function addcomment(newcomment) {
    $.getJSON('scripts/comment.php', { 'table': table, 'comment': newcomment }, function() {});
}

function applycomment() {
    let commentText = commentArea.val();
    if (commentText != '') {
        commentArea.val('');
        addcomment(commentText);
    }
}

function changeGameStatus(status) {
    clearInterval(interval);
    $.getJSON('scripts/change.php', { 'table': table, 'game_status': status }, function(res) {
        for (let i = 0; i < 5; i++) {
            let k = 1;
            if ($('.cards-playing').find('img').eq(i).attr('src') != res.cards[i]) {
                setTimeout(() => {
                    $('.cards-playing').find('img').eq(i).attr('src', res.cards[i]);
                    ion.sound.play('change_status');
                }, k * 1000);
                k++;
            }
        }
        interval = setInterval(loadGame, 1500);
    });
}

function endOfGame() {
    clearInterval(interval);
    $.getJSON('scripts/end.php', { 'table': table }, function(res) {
        rebuyB.removeAttr('disabled');
        if (!isNaN(+res.result)) {
            $('.comment-game').css('opacity', '1').text('Выйгрыш: ' + res.result);
        }
    });
    interval = setInterval(loadGame, 1500);
}

function applyBet(betSum, raise) {
    clearTimeout(timeOut);
    $.getJSON('scripts/bet.php', { 'table': table, 'bet_sum': betSum, 'raise': raise }, function(res) {
        balanceTd.text(+res.balance);
        bank.text(+res.bank_bet);
        $('.comment-game').css('opacity', '1').text('Очередь: ' + res.comment);
        checkB.hide();
        checkB.text('Check');
        foldB.hide();
        betings.hide();
        betB.hide();
        betings.val(0);
    });
    interval = setInterval(loadGame, 1500);
}

function bet() {
    if ((+betings.val() >= minBet) && (+betings.val() <= +balanceTd.text())) {
        ion.sound.play('bet_call');
        addcomment('Bet/Raise');
        applyBet(+betings.val(), 1);
    } else {
        betings.val(minBet);
    }
}

function call() {
    $.getJSON('scripts/game.php', { 'table': table, 'minbet': halfBet }, function(res) {
        let oldBet = +res.player.bet;
        let bet = +res.last_bet;
        let newBet = bet - oldBet;
        ion.sound.play('bet_call');
        addcomment('Call');
        applyBet(newBet, 0);
        checkB.removeClass('green');
    });
}

function allIn() {
    addcomment("All In");
    if (+balanceTd.text() > +lastBet.text()) {
        applyBet(+balanceTd.text(), 1);
    } else {
        applyBet(+balanceTd.text(), 0);
    }
    ion.sound.play('bet_call');
    checkB.removeClass('green');
    betB.removeClass('green').off().on('click', bet);
}

function check() {
    ion.sound.play('check');
    addcomment('Check');
    applyBet(0, 0);
}

function fold() {
    ion.sound.play('fold');
    addcomment('Fold');
    applyBet(-1, 0);
    rebuyB.removeAttr('disabled');
}

function deal() {
    clearInterval(interval);
    clearInterval(clearcomments);
    noBet = false;
    $('td[class^=cards] img').hide();
    $('td[class^=comment]').css('opacity', '0');
    bank.hide();
    lastBet.hide();
    $('td[class^=bet').find('span').text(0).hide();
    $('td[class^=bet').find('img').hide();
    $('.deck').show();
    for (let k = 0; k < 10; k++) {
        let card = $('.deck').find('img').eq(k);
        let tempBalance = +$('td[class^=balance]').eq(k).text() || +$('td[class^=balance]').eq(k - 5).text();
        if (tempBalance >= halfBet) {
            setTimeout(function() {
                card.addClass('animated');
                ion.sound.play('deal');
            }, 350 * k);
        }
    }

    $.getJSON('scripts/deal.php', { 'table': table }, function(res) {
        $('.cards-player img').eq(0).dataAttr('src', res.card1);
        $('.cards-player img').eq(1).dataAttr('src', res.card2);
    });
    setTimeout(() => {
        interval = setInterval(loadGame, 1500);
        addcomment('');
        clearcomments = setInterval(() => {
            addcomment('');
        }, 8000);
        $('.deck').hide();
        $('.deck img').removeClass('animated');
        lastBet.show().text(0);
        bank.show();
        let card_img = $('.cards-player img').eq(0).dataAttr('src');
        $('.cards-player img').eq(0).attr('src', card_img);
        card_img = $('.cards-player img').eq(1).dataAttr('src');
        $('.cards-player img').eq(1).attr('src', card_img);
        for (let i = 0; i < 5; i++) {
            $('.cards-playing').find('img').eq(i).attr('src', 'images/back.svg');
        }
        rebuyB.attr('disabled', true);
    }, 6000);
}

function refresh() {
    location.reload();
}

function showRebuy() {
    $.getJSON('../scripts/get.php', {}, function(res) {
        let balance = +res.result;
        rebuySum.val(limitMin);
        rebuySum.attr('max', balance);
        rebuyBlock.toggle();
    });
}

function rebuy() {
    $.getJSON('../scripts/get.php', {}, function(res) {
        let balance = +res.result;
        if ((rebuySum.val() <= +balance) && (rebuySum.val() <= limitMax)) {
            $.getJSON('scripts/rebuy.php', { 'rebuy': rebuySum.val(), 'table': table }, function(res) {
                balanceTd.text(+res.balance_active);
                if (res.msg == "OK") {
                    rebuyBlock.hide();
                    rebuyB.attr('disabled', true);
                } else {
                    $('.rebuy div').html(res.msg);
                    rebuySum.hide();
                    rebuyOk.text('Назад');
                    rebuyOk.off();
                    rebuyOk.on('click', () => {
                        $.getJSON('../scripts/get.php', {}, function() {
                            rebuySum.show();
                            rebuyOk.text('OK');
                            $('.rebuy div').text('Фишки для игры');
                            rebuyBlock.hide();
                            rebuyOk.off();
                        });
                    });
                }
            });

        } else {
            alert('Недостаточно средств!');
            return;
        }
    });
}

function exit() {
    $.getJSON('scripts/exit_table.php', { 'table': table }, function() {
        window.location.replace('index.php');
    });
}

$('body').on('mousemove', () => { presence(0); });
interval = setInterval(loadGame, 1500);
clearcomments = setInterval(() => {
    addcomment('');
}, 8000);