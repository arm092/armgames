<?php
    session_start();
    include "../../scripts/db.php";
    $login = $_SESSION['login'];
    $bet_sum = $_GET['bet_sum'];
    $table = $_GET['table'];
    $raise = $_GET['raise'];
    $res = $db->query("SELECT * FROM $table WHERE in_play>0");
    $players = $res->fetch_all(MYSQLI_ASSOC);
    for ($i=0;$i<count($players);$i++) {
        if ($players[$i]['login'] == $login) {
            $balance = $players[$i]['balance'];
            $old_bet = $players[$i]['bet'];
            if ($i+1==count($players)) {
                $next_turn_login = $players[1]['login'];
            } else {
                $next_turn_login = $players[$i+1]['login'];
            }
        } elseif ($players[$i]['id']==1) {
            $bank_bet = $players[$i]['bet'];
        }
    }
    if (($balance >= $bet_sum) && ($bet_sum >= 0)) {
        $balance -= $bet_sum;
        $bank_bet += $bet_sum;
        $old_bet += $bet_sum;
    } elseif ($bet_sum < 0) {
        $res = $db->query("UPDATE $table SET in_play = 0 WHERE login = '$login'");
        $bet_sum = 0;
        $res = $db->query("SELECT * FROM $table WHERE in_play>0");
        $players = $res->fetch_all(MYSQLI_ASSOC);
        if (count($players) == 2) {
            $bank = +$players[0]['bet'];
            $balance_user = +$players[1]['balance'] + $bank;
            $one = $players[1]['login'];
            $result = $db->query("UPDATE $table SET balance = '$balance_user', turn = 0, bet = 0 WHERE login = '$one'");
            $res = $db->query("UPDATE $table SET turn = 0 WHERE id > 1");
            $res_bank = $db->query("UPDATE $table SET bet = 0, comment = '', turn = 1, in_play = 1 WHERE id = 1");
            $next_turn_login = 'croupier';
            $bank_bet = 0;
        }
    }
    if ($raise == 0) {
        $winner = +$players[0]['winner'];
    } else {
        $winner = 0;
    }
    $winner++;
    
    $result = $db->query("UPDATE $table SET balance = '$balance', turn = 0, bet = '$old_bet' WHERE login = '$login'");
    $res_bank = $db->query("UPDATE $table SET bet = '$bank_bet', comment = '$next_turn_login', winner = '$winner' WHERE id = 1");
    $res = $db->query("UPDATE $table SET turn = 1 WHERE login = '$next_turn_login'");
    
    $_SESSION['balance_active'] = $balance;
    echo json_encode(array("result" => "OK",
                            "bank_bet" => $bank_bet,
                            "balance" => $balance,
                            "comment" => $next_turn_login,
                            "last_bet" => $bet_sum
                        ));
?>