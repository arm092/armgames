<?php
    session_start();
    include "../../scripts/db.php";
    $table = $_GET['table'];
    $status = $_GET['game_status'];
    $res = $db->query("SELECT * FROM $table WHERE in_play>0");
    $players = $res->fetch_all(MYSQLI_ASSOC);
    $res = $db->query("SELECT * FROM cards");
    $cards = $res->fetch_all(MYSQLI_ASSOC);
    $res_cards = array_fill(0, 5, "images/back.svg");
    $new_status = 1;
    switch ($status) {
        case 'flop':
            $card1 = $players[0]['card1']-1;
            $card2 = $players[0]['card2']-1;
            $card3 = $players[0]['card3']-1;
            $res_cards[0] = $cards[$card1]['img'];
            $res_cards[1] = $cards[$card2]['img'];
            $res_cards[2] = $cards[$card3]['img'];
            $new_status = 2;
            break;
        case 'turn':
            $card1 = $players[0]['card1']-1;
            $card2 = $players[0]['card2']-1;
            $card3 = $players[0]['card3']-1;
            $card4 = $players[0]['card4']-1;
            $res_cards[0] = $cards[$card1]['img'];
            $res_cards[1] = $cards[$card2]['img'];
            $res_cards[2] = $cards[$card3]['img'];
            $res_cards[3] = $cards[$card4]['img'];
            $new_status = 3;
            break;
        case 'river':
            $card1 = $players[0]['card1']-1;
            $card2 = $players[0]['card2']-1;
            $card3 = $players[0]['card3']-1;
            $card4 = $players[0]['card4']-1;
            $card5 = $players[0]['card5']-1;
            $res_cards[0] = $cards[$card1]['img'];
            $res_cards[1] = $cards[$card2]['img'];
            $res_cards[2] = $cards[$card3]['img'];
            $res_cards[3] = $cards[$card4]['img'];
            $res_cards[4] = $cards[$card5]['img'];
            $new_status = 4;
            break;
    }
    if ($players[0]['in_play'] != $new_status) {
        $min_bet = $players[0]['bet'];
        $count = 0;
        for ($i=1; $i < count($players); $i++) { 
            if ($players[$i]['bet'] < $min_bet) {
                $min_bet = $players[$i]['bet'];
            } elseif ($players[$i]['bet'] == $min_bet) {
                $count++;
            }
        }
        if ($count < (count($players) - 1)) {
            $new_bet = $min_bet;
            for ($i=1; $i < count($players); $i++) { 
                if ($players[$i]['bet'] > $min_bet) {
                    $delta_bet = $players[$i]['bet'] - $min_bet;
                    $new_balance = $players[$i]['balance'] + $delta_bet;
                    $players[0]['bet'] -= $delta_bet;
                    $login = $players[$i]['login'];
                    $res = $db->query("UPDATE $table SET balance='$new_balance', bet='$new_bet' WHERE login='$login'");
                }
            }
        }
        $new_bank = $players[0]['bet']; 
        $res = $db->query("UPDATE $table SET bet=0 WHERE id>1");
        $res = $db->query("UPDATE $table SET winner=0, bet='$new_bank' WHERE id=1");
    }
    $res = $db->query("UPDATE $table SET in_play='$new_status' WHERE id=1");
    echo json_encode(array("result" => "OK", "cards" => $res_cards));
?>