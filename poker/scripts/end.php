<?php
    session_start();
    include "../../scripts/db.php";
    $table = $_GET['table'];
    $res = $db->query("SELECT * FROM $table WHERE in_play>0");
    $players = $res->fetch_all(MYSQLI_ASSOC);
    $res = $db->query("SELECT * FROM cards");
    $cards = $res->fetch_all(MYSQLI_ASSOC);
    $players_cards = [];
    $table_cards = [];

    function straight(array $player) {
        $cards_titles = [];
        for ($i=0; $i < count($player) ; $i++) { 
            $cards_titles[] = $player[$i]['title'];
            if ($player[$i]['title'] == 14) {
                $cards_titles[] = 1;
            }
        }
        rsort($cards_titles);
        $count = 0;
        $title = 0;
        $value = 0;
        $combination = false;
        $name = 'Straight';
        $result = [];
        for ($i=1; $i < count($cards_titles); $i++) { 
            if ($cards_titles[$i-1]-$cards_titles[$i] == 1) {
                $count++;
            } elseif ($cards_titles[$i-1] == $cards_titles[$i]) {
                continue;
            } else {
                $count = 0;
            }
            if ($count == 4) {
                $title = $cards_titles[$i-4];
                $i = count($cards_titles);
                $combination = true;
                $value = 5;
            }
        }
        $cards_titles = [];
        for ($i=0; $i < count($player) ; $i++) { 
            $cards_titles[] = $player[$i]['title'];
        }
        rsort($cards_titles);
        $num_value = array_sum($cards_titles);
        $result['num_value'] = $num_value;
        $result[] = $combination;
        $result['title'] = $title;
        $result['value'] = $value;
        $result['name'] = $name;
        return $result;
    }

    function poker_flush(array $player) {
        $cards_suits = [];
        for ($i=0; $i < count($player) ; $i++) { 
            $cards_suits[] = $player[$i]['suit'];
        }
        rsort($cards_suits);
        $for_straight_flush = [];
        $count = 0;
        $title = 0;
        $value = 0;
        $combination = false;
        $name = 'Flush';
        $result = [];
        $suit = 0;
        for ($i=1; $i < count($cards_suits); $i++) { 
            if ($cards_suits[$i-1] == $cards_suits[$i]) {
                $count++;
            } else {
                $count = 0;
            }
            if ($count == 4) {
                $suite = $cards_suits[$i];
                $i = count($cards_suits);
                $combination = true;
                $value = 6;
            }
        }
        for ($i=0; $i < count($player) ; $i++) {
            if ($player[$i]['suit'] == $suit) {
                if ($player[$i]['title'] > $title) {
                    $title = $player[$i]['title'];
                }
                $for_straight_flush[] = $player[$i];
            }
        }
        $cards_titles = [];
        for ($i=0; $i < count($player) ; $i++) { 
            $cards_titles[] = $player[$i]['title'];
        }
        $num_value = array_sum($cards_titles);
        $result['num_value'] = $num_value;
        $result[] = $combination;
        $result['title'] = $title;
        $result['value'] = $value;
        $result['name'] = $name;
        $result['for_str_fl'] = $for_straight_flush;
        return $result;
    }

    function straight_flush(array $player) {
        $title = 0;
        $value = 0;
        $combination = false;
        $name = 'Straight Flush';
        $result = [];
        $flush = poker_flush($player);
        if (($flush[0] == true) && (!empty($flush['for_str_fl']))) {
            $straight = straight($flush['for_str_fl']);
            if ($straight[0] == true) {
                $title = $straight['title'];
                $combination = true;
                $value = 9;
                if ($title == 14) {
                    $name = 'Royal Flush';
                    $value = 10;
                }
            }
        }
        $cards_titles = [];
        for ($i=0; $i < count($player) ; $i++) { 
            $cards_titles[] = $player[$i]['title'];
        }
        $num_value = array_sum($cards_titles);
        $result['num_value'] = $num_value;
        $result[] = $combination;
        $result['title'] = $title;
        $result['value'] = $value;
        $result['name'] = $name;
        return $result;
    }

    function pairs(array $player) {
        $cards_titles = [];
        for ($i=0; $i < count($player) ; $i++) { 
            $cards_titles[] = $player[$i]['title'];
        }
        $cards_titles[] = 0;
        rsort($cards_titles);
        $count = 0;
        $title = 0;
        $value = 0;
        $combination = false;
        $name = '';
        $result = [];
        for ($i=1; $i < count($cards_titles); $i++) { 
            if ($cards_titles[$i-1] == $cards_titles[$i]) {
                $count++;
            } else {
                switch ($count) {
                    case 1:
                        if ($combination == true) {
                            switch ($name) {
                                case 'Pair':
                                    $name = 'Two Pairs';
                                    if ($cards_titles[$i-1] > $title) {
                                        $title = $cards_titles[$i-1];
                                    }
                                    $value = 3;
                                    break;
                                case 'Three of a Kind':
                                    $name = 'Full House';
                                    $title = $cards_titles[$i-1];
                                    $value = 7;
                                    break;
                            }
                        } else {
                            $combination = true;
                            $name = 'Pair';
                            $title = $cards_titles[$i-1];
                            $count = 0;
                            $value = 2;
                        }
                        break;
                    case 2:
                        if ($combination == true) {
                            switch ($name) {
                                case 'Three of a Kind':
                                    if ($cards_titles[$i-1] > $title) {
                                        $title = $cards_titles[$i-1];
                                    }
                                    break;
                                case 'Pair':
                                    $name = 'Full House';
                                    $value = 7;
                                    break;
                            }
                        } else {
                            $combination = true;
                            $name = 'Three of a Kind';
                            $title = $cards_titles[$i-1];
                            $count = 0;
                            $value = 4;
                        }
                        break;
                    case 3:
                        $combination = true;
                        $name = 'Four of a Kind';
                        $title = $cards_titles[$i-1];
                        $value = 8;
                        $count = 0;
                        $i = count($cards_titles);
                        break;
                }
            }
        }
        $cards_titles = [];
        for ($i=0; $i < count($player) ; $i++) { 
            $cards_titles[] = $player[$i]['title'];
        }
        $num_value = array_sum($cards_titles);
        $result['num_value'] = $num_value;
        $result[] = $combination;
        $result['title'] = $title;
        $result['value'] = $value;
        $result['name'] = $name;
        return $result;
    }

    function high_card(array $player) {
        $cards_titles = [];
        for ($i=0; $i < 2 ; $i++) { 
            $cards_titles[] = $player[$i]['title'];
        }
        rsort($cards_titles);
        $title = $cards_titles[0];
        $num_value = $cards_titles[1];
        $value = 1;
        $combination = true;
        $name = 'High Card';
        $result = [];
        $result[] = $combination;
        $result['title'] = $title;
        $result['value'] = $value;
        $result['name'] = $name;
        $result['num_value'] = $num_value;
        return $result;
    }

    // function kicker(array $player, array $combo) {
    //     $cards_titles = [];
    //     for ($i=0; $i < count($player); $i++) { 
    //         $cards_titles[] = $player[$i]['title'];
    //     }
    //     rsort($cards_titles);
    //     return array_slice($cards_titles, 0, 5);
    // }

    function check_combination(array $player) {
        $result = [];
        $combination = straight_flush($player);
        if ($combination[0] == true) {
            $result = $combination;
        } else {
            $combination = pairs($player);
            if (($combination[0] == true) && ($combination['name'] == 'Four of a Kind')) {
                $result = $combination;
            } elseif ($combination['name'] == 'Full House') {
                $result = $combination;
            } else {
                $combination = poker_flush($player);
                if ($combination[0] == true) {
                    $result = $combination;
                } else {
                    $combination = straight($player);
                    if ($combination[0] == true) {
                        $result = $combination;
                    } else {
                        $combination = pairs($player);
                        if ($combination[0] == true) {
                            $result = $combination;
                        } else {
                            $combination = high_card($player);
                            $result = $combination;
                        }
                    }
                }
            }
        }
        return $result;
    }

if ($players[0]['in_play'] == 4) {

    $min_bet = $players[0]['bet'];
        $count = 0;
        for ($i=1; $i < count($players); $i++) { 
            if ($players[$i]['bet'] < $min_bet) {
                $min_bet = $players[$i]['bet'];
            } elseif ($players[$i]['bet'] == $min_bet) {
                $count++;
            }
        }
        if ($count < (count($players) - 1)) {
            $new_bet = $min_bet;
            for ($i=1; $i < count($players); $i++) { 
                if ($players[$i]['bet'] > $min_bet) {
                    $delta_bet = $players[$i]['bet'] - $min_bet;
                    $new_balance = $players[$i]['balance'] + $delta_bet;
                    $players[0]['bet'] -= $delta_bet;
                    $player_name = $players[$i]['login'];
                    $res = $db->query("UPDATE $table SET balance='$new_balance', bet='$new_bet' WHERE login='$player_name'");
                }
            }
        }
        $new_bank = $players[0]['bet']; 
        $res = $db->query("UPDATE $table SET bet=0 WHERE id>1");
        $res = $db->query("UPDATE $table SET winner=0, bet='$new_bank' WHERE id=1");

    $table_cards_str = '';
    for ($i=1; $i < 6 ; $i++) { 
        $card_id = $players[0]['card'. $i];
        $table_cards_str .= $card_id . "  ";
        $table_cards[] = $cards[$card_id-1];
    }
    for ($i=1;$i<count($players); $i++) {
        $card_id = $players[$i]['card1'];
        $players_cards[$i-1][0] = $cards[$card_id-1];
        $card_id = $players[$i]['card2'];
        $players_cards[$i-1][1] = $cards[$card_id-1];
        for ($k=0; $k < count($table_cards); $k++) { 
            $players_cards[$i-1][$k+2] = $table_cards[$k];
        }
    }
    $winners = [];
    $max_num_value = 0;
    $max_combo_index = -1;
    $max_value = 0;
    $max_title = 0;
    $max_name = '';
    for ($i=0;$i<count($players_cards); $i++) {
        $combo = check_combination($players_cards[$i]);
        if ($combo['value'] > $max_value) {
            $max_combo_index = $i;
            $max_num_value = $combo['num_value'];
            $max_value = $combo['value'];
            $max_title = $combo['title'];
            $max_name = $combo['name'];
            $winners = [];
            $winners[0] = $i;
        } elseif ($combo['value'] == $max_value) {
            if ($combo['title'] > $max_title) {
                $max_combo_index = $i;
                $max_num_value = $combo['num_value'];
                $max_value = $combo['value'];
                $max_title = $combo['title'];
                $max_name = $combo['name'];
                $winners = [];
                $winners[0] = $i;
            } elseif ($combo['title'] == $max_title) {
                if ($combo['num_value'] > $max_num_value) {
                        $max_combo_index = $i;
                        $max_num_value = $combo['num_value'];
                        $max_value = $combo['value'];
                        $max_title = $combo['title'];
                        $max_name = $combo['name'];
                        $winners = [];
                        $winners[0] = $i;
                } elseif ($combo['num_value'] == $max_num_value) {
                        $winners[] = $i;
                }
            }
        }
    }
    $winner = [];
    for ($i=0; $i < count($winners); $i++) { 
        $winner[] = $players[$winners[$i]+1];
    }
    $pot = +$players[0]['bet'];
    $bank_percent = $pot * 0.005;
    $bank = +$players[0]['balance'];
    $bank += $bank_percent;
    $pot -= $bank_percent;
    $pot /= count($winner);
    $sum = floor($pot);
    $pot = $sum;
    $winer_login = '';
    $res_bank = $db->query("UPDATE $table SET balance = '$bank', bet = 0, comment = $pot, winner = 0, in_play = 1, turn = 1 WHERE id = 1");
    $res = $db->query("UPDATE $table SET winner=0 WHERE id>1");
    for ($i=0; $i < count($winner); $i++) { 
        $login = $winner[$i]['login'];
        $winer_login .= $login . '  ';
        $balance = +$winner[$i]['balance'];
        $balance += $pot;
        if ($login == $_SESSION['login']) {
            $_SESSION['balance_active'] = $balance;
        }
        $card_1 = +$winner[$i]['card1'];
        $card_2 = +$winner[$i]['card2'];
        $winner[$i]['card1'] = $cards[$card_1-1]['img'];
        $winner[$i]['card2'] = $cards[$card_2-1]['img'];
        $card_1 = $winner[$i]['card1'];
        $card_2 = $winner[$i]['card2'];
        $res = $db->query("UPDATE $table SET balance = '$balance', comment = '$max_name', card3 = '$card_1', card4 = '$card_2', turn = 0, bet = 0, winner = 1 WHERE login = '$login'");
    }
    $res = $db->query("UPDATE $table SET bet=0 WHERE id>1");
    $res = $db->query("UPDATE $table SET in_play=0 WHERE balance<25");
    $players_cards_str = '';
    for ($i=1; $i < count($players); $i++) { 
        $players_cards_str .= $players[$i]['card1'] . "  " . $players[$i]['card2'] . "; ";
    }

    $history = $db->query("INSERT INTO poker_history (table_name, table_cards, players_cards, winer_login, win_combination, win_sum) VALUES ('$table', '$table_cards_str', '$players_cards_str', '$winer_login', '$max_name', '$pot')");

    // echo json_encode(array("result" => "OK", "winner" => $winner, "comment" => $max_name));
}
    $res = $db->query("SELECT * FROM $table WHERE in_play>0");
    $players = $res->fetch_all(MYSQLI_ASSOC);
    $msg = $players[0]['comment'];
    echo json_encode(array("result" => $msg));
?>