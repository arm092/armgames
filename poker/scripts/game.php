<?php
    session_start();
    if (empty($_SESSION['group'])){
        echo ("<html><head><meta http-equiv='Refresh' content='0; URL=index.php'></head></html>");
    };
    $table = $_GET['table'];
    $min_bet = $_GET['minbet']; 
    $login = $_SESSION['login'];
    include "../../scripts/db.php";
    $res = $db->query("SELECT id, login, balance, avatar_url, color, bet, comment, turn, in_play, winner, card3, card4 FROM $table WHERE login != '$login'");
    $res_this = $db->query("SELECT * FROM $table WHERE login = '$login'");
    $player = $res_this->fetch_assoc();
    $players = $res->fetch_all(MYSQLI_ASSOC);
    $players[0]['card3'] = 0;
    $players[0]['card4'] = 0;
    $res = $db->query("SELECT * FROM $table WHERE in_play>0");
    $players_inplay = $res->fetch_all(MYSQLI_ASSOC);
    $res = $db->query("SELECT * FROM $table WHERE turn=1");
    $queue = $res->fetch_assoc();
    $now_turn_login = $queue['login'];
    if ($players[0]['in_play'] == 1) {
        $bet_sum = $min_bet;
    } else {
        $bet_sum = 0;
    }
    for ($i=0;$i<count($players_inplay);$i++) {
        if ($players_inplay[$i]['login'] == $now_turn_login) {
            if (($i-1 > 0) && (+$players_inplay[$i-1]['bet']>0)) {
                $bet_sum = $players_inplay[$i-1]['bet'];
            } elseif (+$players_inplay[count($players_inplay)-1]['bet']>0) {
                $bet_sum = $players_inplay[count($players_inplay)-1]['bet'];
            }
        }
    }
    $card1 = $player['card1'];
    $card2 = $player['card2'];
    if ($card1 > 0) {
        $res_cards = $db->query("SELECT * FROM cards");
        $cards = $res_cards->fetch_all(MYSQLI_ASSOC);
        $card1_img = $cards[$card1-1]['img'];
        $card2_img = $cards[$card2-1]['img'];
    } else {
        $card1_img = 'images/back.svg';
        $card2_img = 'images/back.svg';
    }
    echo json_encode(array("player" => $player,
                            "players" => $players,
                            "coment" => $now_turn_login,
                            "last_bet" => $bet_sum,
                            "card1" => $card1_img,
                            "card2" => $card2_img,
                            "players_inplay" => (count($players_inplay)-2)
                        )
                    );
?>