<?php
    session_start();
    include "../../scripts/db.php";
    $login = $_SESSION['login'];
    $avatar = $_SESSION['avatar'];
    $color = $_SESSION['color'];
    $balance_active = $_SESSION['balance_active'];
    $rebuy_sum = $_GET['rebuy'];
    $table = $_GET['table'];
    $table_limit = [1000, 5000];

    switch ($table) {
        case 'small':
            $table_limit = [1000, 10000];
            break;
        case 'normal':
            $table_limit = [5000, 25000];
            break;
        case 'big':
            $table_limit = [10000, 50000];
            break;
        case 'royal':
            $table_limit = [20000, 100000];
            break;
        default:
            $table_limit = [1000, 5000];
            break;
    }

    $res_user = $db->query("SELECT balance FROM users WHERE login = '$login'");
    $user = $res_user->fetch_assoc();
    if (($rebuy_sum <= $user['balance']) && ($rebuy_sum >= $table_limit[0]) && ($rebuy_sum <= $table_limit[1])) {
        $res_table = $db->query("SELECT * FROM $table");
        $table_res = $res_table->fetch_all(MYSQLI_ASSOC);
        $players = count($table_res);
        for ($i=0;$i<$players;$i++) {
            if ($table_res[$i]['login']==$login) {
               $balance_active = $table_res[$i]['balance'] + $rebuy_sum;
               $balance_new = $user['balance'] - $rebuy_sum;
               $res_active = $db->query("UPDATE users SET balance_active = '$balance_active' WHERE login = '$login'");
               $res_active = $db->query("UPDATE users SET balance = '$balance_new' WHERE login = '$login'");
               $res_active = $db->query("UPDATE $table SET balance = '$balance_active' WHERE login = '$login'");
               $_SESSION['balance'] = $user['balance'];
               $_SESSION['balance_active'] = $balance_active;
               $msg = "OK";
               echo json_encode(array("balance_active" => $balance_active, "balance" => $user['balance'], "msg" => $msg));
               exit;
            }
        }
        if ($players == 6) {
            $balance_active = 0;
            $msg = "Все места за этим столом заняты,</br>попробуйте позже.";
        } else {
            $balance_active += $rebuy_sum;
            $balance_new = $user['balance'] - $rebuy_sum;
            $res_active = $db->query("UPDATE users SET balance_active = '$balance_active' WHERE login = '$login'");
            $res_active = $db->query("UPDATE users SET balance = '$balance_new' WHERE login = '$login'");
            $res = $db->query("INSERT INTO $table (login, balance, avatar_url, color, comment, in_play) VALUES ('$login', '$balance_active', '$avatar', '$color', '', 1)");
            $msg = "OK";
        }
    } else {
        $balance_active = 0;
        $msg = "Введите корректную сумму.</br>Или пополните свой баланс в обслуживающей кассе.";
    }
    $_SESSION['balance'] = $user['balance'];
    $_SESSION['balance_active'] = $balance_active;
    echo json_encode(array("balance_active" => $balance_active, "balance" => $user['balance'], "msg" => $msg));
?>