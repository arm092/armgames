<html>
    <head>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="images/favico.ico" type="image/x-icon">
        <link rel="stylesheet" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Marmelad|Neucha" rel="stylesheet">
        <title>Регистрация</title>
    </head>
    <body>
        <header>
        <div class='welcome'>
            <span>
                Регистрация
            </span>
        </div>
        </header>
        <main class="reg">
            <form action="scripts/save_user.php" method="post">
                <div class='text'>Введите данные для регистрации:</div>
                <table>
                    <tr>
                    <td><label for='login'>Логин:</label></td>
                    <td><input type='text' size='15' maxlength="15"  id='login' name='login' required></td>
                    </tr>
                    <tr>
                    <td><label for='pswd'>Пароль:</label></td>
                    <td><input type='password' size='15' maxlength="15"  id='pswd' name='password' required></td>
                    </tr>
                    <tr>
                    <td><label for='num'>Номер тел.: +374</label></td>
                    <td><input type='text' size='15' maxlength="8"  id='num' name='number' required></td>
                    </tr>
                    <tr>
                    <td><label for='admin-id'>Обслуживающая касса:</label></td>
                    <td><select name="admin_id" id="admin-id" size=1 required>
                        <option disabled selected>Выберети кассу</option>
                        <option value="2">Чарбах</option>
                        <option value="3">Каяран</option>
                    </select></td>
                    </tr>
                    <tr>
                    <td><div class="text">Выберете аватар:</div></td>
                    <td><div class="avatars">
                        <?php
                            include ("scripts/db.php");
                            $result = $db->query("SELECT * FROM avatars");
                            $avatars = $result->fetch_all(MYSQLI_ASSOC);
                            for ($i=0, $avatars_length=count($avatars); $i<$avatars_length; $i++) {
                                $key = $avatars[$i];
                                echo "<span><input type='radio' name='avatar_id' value=".$key['id']." id='avatar".$key['id']."' required><label for='avatar".$key['id']."'><img src='".$key['url']."'></label></span>";
                            };
                        ?>
                    </div></td>
                    </tr>
                </table>
                <input style="cursor: pointer" type="submit" name="submit" value="Зарегистрироваться">
            </form>
        </main>
    </body>
    </html>