<!DOCTYPE html>

<?php
header("Content-Type: text/html; charset=utf-8");    
    session_start();
    if (!empty($_SESSION['group'])){
        if ($_SESSION['group'] == 'admins' or $_SESSION['group'] == 'global') {
            echo ("<html><head><meta http-equiv='Refresh' content='0; URL=../admin.php'></head></html>");
        };
    };
?>
<html>

<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="images/favico.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Marmelad|Neucha" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css">
    <script src='../js/jquery-min.js'></script>
    <script src="../js/jquery.json.min.js"></script>
    <script src='../js/jquery.transform2d.js'></script>
    <title>Играть в Рулетку</title>
</head>

<body>
    <section class="need-rotate">
        <div>
            <span>Переверните девайс, чтобы играть.</span>
            <img src='../images/rotate.png'>
        </div>
    </section>
    <header>
        <div class='info'>
            <div>
                <span class='bold rules-button'>Коэффициенты</span>
                <img src="images/rules.png" id="rules">
            </div>
            <div id="draws-button">
                <span>История</span>
            </div>
        </div>
        <div class="i-frame-draws">
            <iframe class="draws" src="loading.html"></iframe>
            <button id="close-frame-draws"></button>
        </div>
        <div class='welcome'>
            <span>
                Игра<br>"РУЛЕТКА"
            </span>
        </div>
        <div class='balance'>
            <div>
                <?php
                    if (empty($_SESSION['login'])) {
                        echo "<b>Ваш игровой счет</b><br>Баланс: <span id='money'>0</span> драм";
                    }
                    else {
                        include "../scripts/db.php";
                        $user = $_SESSION['login'];
                        $res = $db->query("SELECT * FROM users WHERE login='$user'");
                        $mydata = $res->fetch_assoc();
                        $_SESSION['balance'] = $mydata['balance'];
                        $avatar_id = $mydata['avatar_id'];
                        $avatar_res = $db->query("SELECT img FROM avatars WHERE id='$avatar_id'");
                        $avatar = $avatar_res->fetch_assoc();
                        $_SESSION['avatar'] = $avatar['img'];
                        echo "<div class='login-info flex'><div><img src='".$_SESSION['avatar']."'></div><div><b id='user-login'>".$_SESSION['login']."</b><br>Баланс: <span id='money'>".$_SESSION['balance']."</span> драм</div><div><a href='../scripts/exit.php' class='link-button' id='exit'>Выйти</a></div></div>";
                    }
                ?>
            </div>
        </div>
    </header>
    <div class="main">
        <div id='user-name-block'>
            <?php
                if (empty($_SESSION['login']) or empty($_SESSION['id'])) {
                    echo "<form action='../scripts/test_user.php' method='post'><label for='login' class='text'>Имя пользователя:</label><br><input type='text' size='15' max-length='15' id='user-name' name='login'><br><label for='password' class='text'>Пароль:</label><br><input type='password' size='15' max-length='15' id='user-name' name='password'><br><br><input type='submit' style='cursor: pointer' name='submit' value='Войти'> <a href='../reg.php' class='link-button'>Регистрация</a></form><br>Вы вошли на сайт, как гость<br><a href='#' style='color: white; font-size: 0.7em'>Эта ссылка  доступна только зарегистрированным пользователям</a>";
                }
                else {
                    echo "Вы вошли на сайт, как ".$_SESSION['login']."<br><button id='play'>Играть</button>";
                }
            ?>
        </div>
        <?php
            include ("../scripts/db.php");
            $res2_bank = $db->query("SELECT balance_active FROM admins WHERE id='1'");
            $mybank = $res2_bank->fetch_assoc();
            $data = $mybank['balance_active'];
            $_SESSION['bank'] = $data;
        ?>
        <div class="game-block">
        <div class="game flex">
            <div class="game-table" id="game-field">
                <table>
                    <tr>
                        <td rowspan="3" class="bg-green">0</td>
                        <td class="bg-red">3</td>
                        <td class="bg-black">6</td>
                        <td class="bg-red">9</td>
                        <td class="bg-red">12</td>
                        <td class="bg-black">15</td>
                        <td class="bg-red">18</td>
                        <td class="bg-red">21</td>
                        <td class="bg-black">24</td>
                        <td class="bg-red">27</td>
                        <td class="bg-red">30</td>
                        <td class="bg-black">33</td>
                        <td class="bg-red">36</td>
                        <td rowspan="3" class="bg-green">00</td>
                    </tr>
                    <tr>
                        <td class="bg-black">2</td>
                        <td class="bg-red">5</td>
                        <td class="bg-black">8</td>
                        <td class="bg-black">11</td>
                        <td class="bg-red">14</td>
                        <td class="bg-black">17</td>
                        <td class="bg-black">20</td>
                        <td class="bg-red">23</td>
                        <td class="bg-black">26</td>
                        <td class="bg-black">29</td>
                        <td class="bg-red">32</td>
                        <td class="bg-black">35</td>
                    </tr>
                    <tr>
                        <td class="bg-red">1</td>
                        <td class="bg-black">4</td>
                        <td class="bg-red">7</td>
                        <td class="bg-black">10</td>
                        <td class="bg-black">13</td>
                        <td class="bg-red">16</td>
                        <td class="bg-red">19</td>
                        <td class="bg-black">22</td>
                        <td class="bg-red">25</td>
                        <td class="bg-black">28</td>
                        <td class="bg-black">31</td>
                        <td class="bg-red">34</td>
                    </tr>
                    <tr>
                        <td rowspan="3" class="bg-red">RED</td>
                        <td colspan="4" class="bg-blue">1-12</td>
                        <td colspan="4" class="bg-blue">13-24</td>
                        <td colspan="4" class="bg-blue">25-36</td>
                        <td rowspan="3" class="bg-black">BLK</td>
                    </tr>
                    <tr>
                        <td colspan="6" class="bg-blue">EVEN</td>
                        <td colspan="6" class="bg-blue">ODD</td>
                    </tr>
                    <tr>
                        <td colspan="12" class="bg-green">GREEN</td>
                    </tr>
                </table>
                <table id="bet-table">
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="bet-info">
                            Сумма ставок: <span id="all-bet-sum">0</span> драм
                        </td>
                        <td class="bet-info">
                            Сумма выйгрыша: <span id="all-win-sum">0</span> драм
                        </td>
                    </tr>
                </table>
            </div>
            <div id='betting' >
                <div class='text'>Выберете сумму ставки:</div>
                <select id="bet">
                    <option value="50" selected>50 драм</option>
                    <option value="100">100 драм</option>
                    <option value="200">200 драм</option>
                    <option value="500">500 драм</option>
                    <option value="1000">1000 драм</option>
                    <option value="3000">3000 драм</option>
                </select>
            </div>
            <div class="time-block">
            <div class="time-bet flex-col">
                <div id="timer">
                    <div class="timing black">
                        <span id="time">0</span>
                    </div>
                </div>
                <div id="bet-ind" class="green" >
                    <div id="bet-button">
                        <span>BET</span>
                    </div>
                </div>
                <div id="draw-num">
                    <span class="text">Тираж: </span><span class="text" id="draw-id">00000</span>
                </div>
            </div>
            </div>
            <div class="roulette">
                <div class="ball">
                    <img src="images/ball.png" alt="">
                </div>
                <div id="win-number"></div>
            </div>
        </div>
    </div>
    </div>
    <script src="js/script.js"></script>
</body>
</html>