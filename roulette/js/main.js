$(document).ready(function() {
    var gamer = new Array();
    var winNumber;
    var betNumbers = 0;
    var winSum = 0;
    var coin = [];
    var bet = new Array();
    var table = $('#game-field');
    var betText = $('.bet-text');
    var betTable = $('.bet-box');
    var balance = $('#money').text();
    var makeBet = $('#make-bet');
    makeBet.bind('click', MakeBet);
    var start = $('#start');
    start.bind('click', ShowBetting);
    var clean = $('#clean');
    clean.bind('click', Clean);
    var cleanAll = $('#clean-all');
    cleanAll.bind('click', EmptyField);
    var add = $('#add');
    add.bind('click', Add);
    var rules = $('.rules-button');
    rules.bind('click', ShowRules);
    var play = $('#play');
    play.bind('click', Play);
    var draws = $('#draws-button span');
    draws.bind('click', ShowDraws);
    var userNameBlock = $('#user-name-block');
    var betting = $('#betting');
    var betInput = $('#bet');
    var newGame = $('#new-game');
    var welcome = $('.welcome');
    var gameBlock = $('#game');
    var timer = $('#timer div');
    var time = $('#time');
    var now;
    var nextDrawTime;
    var fresh;

    // function ShowRules() {
    //     $('#rules').toggle();
    // }

    // function ShowDraws() {
    //     $('.i-frame-draws').show(10, function() {
    //         $(this).unbind('click', ShowDraws);
    //         setTimeout(function() {
    //             $('iframe.draws').attr('src', '../../history_roulette.php');
    //         }, 1500);
    //         $('#close-frame-draws').click(function() {
    //             $('.i-frame-draws').hide(50, function() {
    //                 $('iframe.draws').attr('src', '../loading.html');
    //                 $(this).bind('click', ShowDraws);
    //             });
    //         });
    //     });
    // }

    function EmptyField() {
        for (let i = 0; i < 4; i++) {
            for (let j = 0; j < 10; j++) {
                let td = betTable.eq(i).find('tr').eq(0).find('td').eq(j);
                td.text('');
                td.removeClass('red green').addClass('black');
            }
        };
        Clean();
        clean.show();
        cleanAll.show();
        add.show();
        start.text("Ставка");
        bet = new Array();
    };

    function Clean() {
        table.html("<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");
        let number = 1;
        table.each(function() {
            let $table = $(this);
            $('td', $table).each(function() {
                let $td = $(this);
                $td.text(number);
                $td.removeClass('yellow red').addClass('black');
                $td.bind('click', UserTurn);
                number++;
            })
        });
        gamer = [];
        winNumbers = [];
        coin = [0, 0, 0, 0];
        gameText.text("");
    };

    function UserTurn() {
        if (gamer.length < 10) {
            $(this).removeClass('black').addClass('red');
            $(this).unbind('click', UserTurn);
            gamer.push(this.innerHTML);
            gameText.text("Выбрано " + gamer.length + " номеров");
        }
    };

    function Add() {
        if ((gamer.length <= 10) && (gamer.length > 0)) {
            if (bet.length < 4) {
                bet.push(gamer);
                gamer = [];
            }
            let j = bet.length - 1;
            for (let i = 0; i < bet[j].length; i++) {
                let td = betTable.eq(j).find('tr').eq(0).find('td').eq(i);
                td.text(bet[j][i]);
                td.removeClass('black').addClass('red');
            };
            Clean();
        }
    }

    function Play() {
        userNameBlock.hide();
        welcome.hide();
        gameBlock.show();
        Clean();
        betInput.attr('max', balance);
        let balDigits = (balance + '').replace('.', '').length;
        if (balDigits > 5 && balance > 0) {
            betInput.width(balDigits / 2 + 2 + 'em');
        }
        $.getJSON('../scripts/get.php', {}, function(res) {
            balance = +res.result;
            nextDrawTime = +res.draw_time + 121;
            now = new Date();
            nextDrawTime = Math.round((nextDrawTime * 1000 - (+now)) / 1000)
            time.text(nextDrawTime);
            $('#money').text(balance);
            let nextId = +res.draw_id;
            $('#draw-id').text(nextId);
        }).then(function(params) {
            nextDrawTime = time.text();
            if ((start.is(':hidden')) && (!($('#win-table').find('tr').eq(0).find('td').eq(21).hasClass('animated')))) {
                start.show();
                clean.show();
                cleanAll.show();
                add.show();
            }
            fresh = setInterval(() => {
                if (nextDrawTime >= 2) {
                    if ((nextDrawTime < 100) && (!($('#win-table').find('tr').eq(0).find('td').eq(21).hasClass('animated')))) {
                        if (timer.is(':hidden')) { timer.show(); }
                        time.text(nextDrawTime);
                        if (nextDrawTime > 20) {
                            timer.removeClass().addClass('timing green');
                        } else if (nextDrawTime > 10) {
                            timer.removeClass().addClass('timing yellow');
                        } else if (nextDrawTime > 1) {
                            timer.removeClass().addClass('timing red');
                        }
                    } else { timer.hide(); }
                    nextDrawTime--;
                } else {
                    timer.hide();
                    clearInterval(fresh);
                }
            }, 1000);
        }).then(function(params) {
            setTimeout(() => {
                clearInterval(fresh);
                if (start.is(':hidden')) {
                    Clean();
                } else {
                    EmptyField();
                }
                timer.hide();
                coin = [0, 0, 0, 0];
                $.getJSON('scripts/game.php', { 'draw_id': +$('#draw-id').text() }, function(res) {
                    $('#game-field').each(function() {
                        let $table = $(this);
                        $('td', $table).each(function() {
                            $(this).unbind('click', UserTurn);
                        });
                    });
                    start.hide();
                    clean.hide();
                    cleanAll.hide();
                    add.hide();
                    winNumbers = res.win_numbers;
                    coin = res.coin;
                    balance = +res.balance;
                    for (let k = 0; k < winNumbers.length; k++) {
                        for (let i = 0; i < 8; i++) {
                            for (let j = 0; j < 10; j++) {
                                let td = table.find('tr').eq(i).find('td').eq(j);
                                if (td.text() == winNumbers[k]) {
                                    setTimeout(function() {
                                        td.removeClass('black').addClass('yellow');
                                    }, 1200 * k + 1000);
                                }
                            }
                        }
                    };
                    for (let k = 0; k < winNumbers.length; k++) {
                        let ball = $('#win-table').find('tr').eq(0).find('td').eq(winNumbers.length - k - 1);
                        ball.text(winNumbers[k]);
                        setTimeout(function() {
                            ball.addClass('yellow animated');
                        }, 1200 * k - 50 * k);
                    };
                    for (let i = 0; i < bet.length; i++) {
                        for (let k = 0; k < 22; k++) {
                            for (let j = 0; j < bet[i].length; j++) {
                                let td = betTable.eq(i).find('tr').eq(0).find('td').eq(j);
                                if (td.text() == winNumbers[k]) {
                                    setTimeout(function() {
                                        td.removeClass('red').addClass('green');
                                    }, 1200 * k + 1000);
                                }
                            }
                        }
                    };
                    setTimeout(function() {
                        for (let i = 0; i < bet.length; i++) {
                            if (bet[i].length > 0) {
                                betText.eq(i).text(coin[i] + ' совпадений из ' + bet[i].length);
                            }
                        }
                        $.getJSON('../scripts/get.php', {}, function(res) {
                            balance = +res.result;
                            $('#money').text(balance);
                            let nextId = +res.draw_id;
                            $('#draw-id').text(nextId);
                        });
                        balance = +$('#money').text();
                        Play();
                        //timer.hide();
                    }, 28000);
                })
            }, nextDrawTime * 1000 + 1500);
        }).then(function(params) {
            for (let k = 0; k < 22; k++) {
                let ball = $('#win-table').find('tr').eq(0).find('td').eq(k);
                if (ball.hasClass('animated')) {
                    if (k == 21) {
                        setTimeout(function() {
                            ball.removeClass('animated');
                            EmptyField();
                            for (let i = 0; i < 4; i++) {
                                betText.eq(i).text("");
                            }
                            timer.show();
                            start.show();
                            clean.show();
                            cleanAll.show();
                            add.show();
                        }, 500 * k);
                    } else {
                        setTimeout(function() {
                            ball.removeClass('animated');
                        }, 500 * k);
                    }
                }
            }
        });
    };

    function ShowBetting() {
        $.getJSON('../scripts/get.php', {}, function(res) {
            balance = +res.result;
            $('#money').text(balance);
            betInput.attr('max', balance);
            let nextId = +res.draw_id;
            $('#draw-id').text(nextId);
        });
        if (bet.length > 0) {
            betting.toggle();
        }
    }

    function MakeBet() {
        nextDrawTime = +time.text();
        if (nextDrawTime > 3) {
            $.getJSON('../scripts/get.php', {}, function(res) {
                balance = +res.result;
                $('#money').text(balance);
                betInput.attr('max', balance);
                let nextId = +res.draw_id;
                $('#draw-id').text(nextId);
            }).then(function mb(params) {
                balance = $('#money').text();
                betSum = +betInput.val();
                if (betSum > balance) {
                    betInput.val(balance);
                };
                let betText = $('div.text');
                if ((betSum.isNaN) || (!((betSum > 0) && (betSum <= balance)))) {
                    betInput.val(100);
                    betSum = 0;
                    betText.text('Введите корректную сумму ставки');
                    $.getJSON('../scripts/get.php', {}, function(res) {
                        balance = +res.result;
                        $('#money').text(balance);
                    });
                    return;
                } else if (betSum > balance) {
                    betText.text("На Вашем счету недостаточно средств!");
                    return;
                };
                Clean();
                if ((bet[0].length <= 10) && (bet[0].length > 0)) {
                    $('#game-field').each(function() {
                        let $table = $(this);
                        $('td', $table).each(function() {
                            $(this).unbind('click', UserTurn);
                        });
                    });
                    balance -= betSum;
                    $('#money').text(balance);
                    betting.hide();
                    betText.text('Ваша ставка:');
                    start.hide();
                    clean.hide();
                    cleanAll.hide();
                    add.hide();
                    $.getJSON('scripts/bet.php', { 'bet_sum': betSum, 'bet[][]': bet, 'draw_id': +$('#draw-id').text() }, function(res) {
                        $('#money').text(res.result);
                    });
                }
            });
        } else {
            alert('Невозможно принять ставку!');
            betting.toggle();
            EmptyField();
        }
    }
    EmptyField();
});