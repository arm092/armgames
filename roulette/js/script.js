var betObj = {};


jQuery.fn.rotate = function(degrees) {
    $(this).css({
        '-webkit-transform': 'rotate(' + degrees + 'deg)',
        '-moz-transform': 'rotate(' + degrees + 'deg)',
        '-ms-transform': 'rotate(' + degrees + 'deg)',
        'transform': 'rotate(' + degrees + 'deg)'
    });
    return $(this);
};

$(document).ready(function() {
    var dozen = '';
    var type = '';
    var color = '';
    var winNumber;
    var winSum = 0;
    var betSum = 0;
    var table = $('#game-field');
    var balance = $('#money').text();

    var userNameBlock = $('#user-name-block');
    var betting = $('#betting');

    var timer = $('#timer div');
    var time = $('#time');
    var now;
    var nextDrawTime;
    var fresh;
    $('.rules-button').on('click', showRules);
    $('#draws-button').on('click', showDraws);
    $('#play').on('click', play);
    $('#bet-button').on('click', bet);
    for (let i = 0; i < 46; i++) {
        $('#game-field td').eq(i).on('click', addBet)
    }

    function showRules() {
        $('#rules').toggle();
    }

    function showDraws() {
        $('.i-frame-draws').show(10, function() {
            $(this).off();
            setTimeout(function() {
                $('iframe.draws').attr('src', '../history_roulette.php');
            }, 1500);
            $('#close-frame-draws').click(function() {
                $('.i-frame-draws').hide(50, function() {
                    $('iframe.draws').attr('src', 'loading.html');
                    $(this).on('click', showDraws);
                });
            });
        });
    }

    function emptyField() {
        betObj = {};
        for (let i = 0; i < 46; i++) {
            $('#game-field td').eq(i).removeClass('bg-select').on('click', addBet);
        }
        for (let i = 0; i < 10; i++) {
            $('#bet-table td').eq(i).text('').css('background-color', 'transparent').off().removeClass('bg-select');
        }
        $('#all-bet-sum').text(0);
        $('#all-win-sum').text(0);
    }

    function play() {
        $('#user-name-block').hide();
        $('.game-block').show();
        $.getJSON('../scripts/get.php', {}, function(res) {
            balance = +res.result;
            nextDrawTime = +res.roulette_draw_time + 121;
            now = new Date();
            nextDrawTime = Math.round((nextDrawTime * 1000 - (+now)) / 1000);
            time.text(nextDrawTime);
            $('#money').text(balance);
            let nextId = +res.roulette_draw;
            $('#draw-id').text(nextId);
        }).then(() => {
            nextDrawTime = time.text();
            fresh = setInterval(() => {
                if (nextDrawTime >= 2) {
                    if (nextDrawTime < 100 && ($('.roulette').is(':hidden'))) {
                        if (timer.is(':hidden')) { timer.show(); }
                        time.text(nextDrawTime);
                        if (nextDrawTime > 20) {
                            timer.removeClass().addClass('timing green');
                        } else if (nextDrawTime > 10) {
                            timer.removeClass().addClass('timing yellow');
                        } else if (nextDrawTime > 1) {
                            timer.removeClass().addClass('timing red');
                        }
                    } else { timer.hide(); }
                    nextDrawTime--;
                } else {
                    timer.hide();
                    clearInterval(fresh);
                }
            }, 1000);
        }).then(() => {
            setTimeout(() => {
                clearInterval(fresh);
                if (!$('#bet-ind').is(':hidden')) {
                    emptyField();
                }
                $('.time-block').hide();
                $('#betting').hide();
                $.getJSON('scripts/game.php', { 'draw_id': +$('#draw-id').text() }, function(res) {
                    $('td').each(function() {
                        $(this).off();
                    });
                    winNumber = +res.win_number;
                    if (winNumber >= 1 && winNumber <= 12) {
                        dozen = '1-12';
                    } else if (winNumber >= 13 && winNumber <= 24) {
                        dozen = '13-24';
                    } else if (winNumber >= 25 && winNumber <= 36) {
                        dozen = '25-36';
                    }
                    if ((winNumber % 2) == 0) {
                        type = 'EVEN';
                    } else {
                        type = 'ODD';
                    }
                    if (winNumber == 37) {
                        winNumber = '00';
                    }
                    color = res.win_color;
                    rotate = res.rotate;
                    balance = +res.balance;
                    winSum = +res.win_sum;
                    $('.roulette').show();
                    setTimeout(() => {
                        let count = 0;
                        for (let i = 92; i < 1081 + rotate; i++) {
                            count++;
                            setTimeout(() => {
                                $('.ball').rotate(i);
                            }, 500 + count * 11 + count)
                        }
                        setTimeout(() => {
                            // $('.ball').rotate(1080 + rotate);
                            let bg = '';
                            switch (color) {
                                case 'RED':
                                    bg = 'bg-red';
                                    break;
                                case 'BLK':
                                    bg = 'bg-black';
                                    break;
                                case 'GREEN':
                                    bg = 'bg-green';
                                    break;
                            }
                            $('#win-number').text(winNumber).removeClass().addClass(bg);

                            setTimeout(() => {
                                $('#win-number').show();
                                $('#all-win-sum').text(winSum);
                                for (let i = 0; i < 10; i++) {
                                    let textBet = $('#bet-table td').eq(i).text();
                                    if (textBet != '') {
                                        let bet = textBet.split(':')[0];
                                        if (bet == winNumber || bet == color || bet == dozen || bet == type) {
                                            $('#bet-table td').eq(i).addClass('bg-select');
                                        }
                                    }
                                }

                            }, 3000);
                        }, 600 + count * 10);
                    }, 300);
                    setTimeout(() => {
                        $.getJSON('../scripts/get.php', {}, function(res) {
                            balance = +res.result;
                            $('#money').text(balance);
                            let nextId = +res.roulette_draw;
                            $('#draw-id').text(nextId);
                        });
                        balance = +$('#money').text();
                        emptyField();
                        $('#win-number').hide();
                        $('.roulette').hide();
                        $('.time-block').show();
                        $('#bet-ind').show();
                        $('#betting').show();
                        play();
                    }, 28000);
                })
            }, nextDrawTime * 1000 + 2500);
        })
    }

    function bet() {
        if (Object.keys(betObj).length < 1) {
            alert('Вы не сделали никаких ставок!');
            return;
        } else {
            nextDrawTime = +time.text();
            if (nextDrawTime > 3) {
                $.getJSON('../scripts/get.php', {}, function(res) {
                    balance = +res.result;
                    $('#money').text(balance);
                    let nextId = +res.roulette_draw;
                    $('#draw-id').text(nextId);
                }).then(() => {
                    balance = $('#money').text();
                    betSum = +$('#all-bet-sum').text();
                    if (betSum > balance) {
                        alert('Сумма ставок больше вашего баланса. Удалите несколько ставок и попробуйте снова.');
                        $.getJSON('../scripts/get.php', {}, function(res) {
                            balance = +res.result;
                            $('#money').text(balance);
                        });
                        return;
                    } else {
                        $('#bet-ind').hide();
                        $('#betting').hide();
                        for (let i = 0; i < 46; i++) {
                            $('#game-field td').eq(i).off();
                        }
                        for (let i = 0; i < 10; i++) {
                            $('#bet-table td').eq(i).off();
                        }
                        balance -= betSum;
                        $('#money').text(balance);
                        $.getJSON('scripts/bet.php', { 'bet_sum': betSum, 'bet': betObj, 'draw_id': +$('#draw-id').text() }, function(res) {
                            $('#money').text(res.result);
                        });
                    }
                });
            } else {
                alert('Невозможно принять ставку!');
                emptyField();
            }
        }
    }

    function addBet() {
        if (Object.keys(betObj).length < 10) {
            $(this).off();
            let bgColor = $(this).css('background-color');
            let bet = $(this).text();
            let betSum = +$('#bet').val();
            $(this).addClass('bg-select');
            for (let i = 0; i < 11; i++) {
                let elem = $('#bet-table td:not(.bet-info)').eq(i);
                if (elem.text() == '') {
                    elem.css('background-color', bgColor).text(bet + ': ' + betSum + ' драм').on('click', delBet).attr('title', 'Удалить ставку');
                    i = 12;
                }

            }
            if (bet == '00') {
                bet = '37';
            }
            betObj[bet] = betSum;
            let oldBetSum = +$('#all-bet-sum').text();
            let newBetSum = oldBetSum + betSum;
            $('#all-bet-sum').text(newBetSum);
        } else {
            alert('Все ставки уже сделаны!')
        }
    }

    function delBet() {
        let textBet = $(this).text();
        let bet = textBet.split(':')[0];
        let betSum = textBet.split(':')[1];
        betSum = +betSum.split('драм')[0];
        $(this).text('').css('background-color', 'transparent').off();
        delete betObj[bet];
        for (let i = 0; i < 46; i++) {
            if ($('#game-field td').eq(i).text() == bet) {
                $('#game-field td').eq(i).removeClass('bg-select').on('click', addBet);
            }
        }
        let oldBetSum = +$('#all-bet-sum').text();
        let newBetSum = oldBetSum - betSum;
        $('#all-bet-sum').text(newBetSum);
    }
});