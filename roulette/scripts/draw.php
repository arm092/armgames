<?php
include "../../scripts/db.php";
$draws_new = $db->query("SELECT * FROM roulette_draws ORDER BY id DESC LIMIT 10000");
$draws_new_id = $draws_new->fetch_assoc();
$last_id = $draws_new_id['id'] + 1;
$bal_bank = $db->query("SELECT balance_active FROM admins WHERE id = '1'");
$mybank = $bal_bank->fetch_assoc();
$bank = $mybank['balance_active'];
$bets = $db->query("SELECT * FROM roulette_bets WHERE draw_id='$last_id' ");
$bets_res = $bets ? $bets->fetch_all(MYSQLI_ASSOC) : [];
$red = [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36];
$black = [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35];
$green = [0, 37];

$kayf = 0;
$win_sum = [];
$winings = 0;
$bet_sum = 0;
if (!empty($bets_res[0]['draw_id']) && !empty($bets_res)) {
    for ($i = 0; $i < count($bets_res); $i++) {
        $bet_sum += $bets_res[$i]['bet_sum'];
    }

    $user_games = $db->query("SELECT * FROM roulette_draws ORDER BY id DESC LIMIT 10000");
    $user_win_games = $db->query("SELECT * FROM roulette_draws WHERE win_sum > bet_sum ");
    $res_games = $user_games->fetch_all(MYSQLI_ASSOC);
    $games = count($res_games);
    $res_win_games = $user_win_games->fetch_all(MYSQLI_ASSOC);
    $win_games = count($res_win_games);
    $win_number = -1;
    $win_color = '';
    $win_dozen = '';
    $win_type = '';
    do {
        do {
            $win_number = rand(0, 37);
            if (in_array($win_number, $red)) {
                $win_color = 'RED';
            } elseif (in_array($win_number, $black)) {
                $win_color = 'BLK';
            } elseif (in_array($win_number, $green)) {
                $win_color = 'GREEN';
            }
            if ($win_number >= 1 && $win_number <= 12) {
                $win_dozen = '1-12';
            } elseif ($win_number >= 13 && $win_number <= 24) {
                $win_dozen = '13-24';
            } elseif ($win_number >= 25 && $win_number <= 36) {
                $win_dozen = '25-36';
            }
            if (($win_number % 2) == 0) {
                $win_type = 'EVEN';
            } else {
                $win_type = 'ODD';
            }
            for ($j = 0; $j < count($bets_res); $j++) {
                if ($bets_res[$j]['number'] == $win_number) {
                    $win_sum[$j] += 36 * $bets_res[$j]['bet_sum'];
                }
                if ($bets_res[$j]['color'] == $win_color) {
                    if ($win_color == 'GREEN') {
                        $win_sum[$j] += 18 * $bets_res[$j]['bet_sum'];
                    } else {
                        $win_sum[$j] += 2 * $bets_res[$j]['bet_sum'];
                    }
                }
                if ($bets_res[$j]['type'] == $win_type) {
                    $win_sum[$j] += 2 * $bets_res[$j]['bet_sum'];
                }
                if ($bets_res[$j]['dozen'] == $win_dozen) {
                    $win_sum[$j] += 3 * $bets_res[$j]['bet_sum'];
                }
            }
            $winings = 0;
            for ($i = 0; $i < count($bets_res); $i++) {
                $win_sum[$i] = round($win_sum[$i]);
                $winings += $win_sum[$i];
            }
        } while ($winings > $bank);
        if ($win_games > 0.2 * $games) {
            $winings *= $bank;
            $winings++;
        }
    } while ($winings > $bank);
    for ($i = 0; $i < count($bets_res); $i++) {
        $bet_id = $bets_res[$i]['id'];
        $bet_res = $db->query("UPDATE roulette_bets SET win_sum = '$win_sum[$i]' WHERE id = '$bet_id'");
    }
} else {
    $win_number = rand(0, 37);
    if (in_array($win_number, $red)) {
        $win_color = 'RED';
    } elseif (in_array($win_number, $black)) {
        $win_color = 'BLK';
    } elseif (in_array($win_number, $green)) {
        $win_color = 'GREEN';
    }
    if ($win_number >= 1 && $win_number <= 12) {
        $win_dozen = '1-12';
    } elseif ($win_number >= 13 && $win_number <= 24) {
        $win_dozen = '13-24';
    } elseif ($win_number >= 25 && $win_number <= 36) {
        $win_dozen = '25-36';
    }
    if (($win_number % 2) == 0) {
        $win_type = 'EVEN';
    } else {
        $win_type = 'ODD';
    }
}
$time = time();
$win_res = $db->query("INSERT INTO roulette_draws (draw_time, win_number, color, bet_sum, win_sum) VALUES ('$time', '$win_number', '$win_color', '$bet_sum', '$winings')");
$bank -= $winings;
$res_bank = $db->query("UPDATE admins SET balance_active = '$bank' WHERE id = '1'");
