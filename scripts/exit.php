<?php
    session_start();
    unset($_SESSION['password']);
    unset($_SESSION['login']); 
    unset($_SESSION['id']);
    unset($_SESSION['balance']);
    unset($_SESSION['bank']);
    unset($_SESSION['group']);
    unset($_SESSION['admin_id']);
    exit("<html><head><meta http-equiv='Refresh' content='0; URL=../index.php'></head></html>"); // отправляем пользователя на главную страницу.
?>