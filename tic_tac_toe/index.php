<!DOCTYPE HTML>
<?php
    session_start();
    if (!empty($_SESSION['group'])){
        if ($_SESSION['group'] == 'admins' or $_SESSION['group'] == 'global') {
            echo ("<html><head><meta http-equiv='Refresh' content='0; URL=../admin.php'></head></html>");
        };
    };
?>
<html>

<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="images/favico.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Marmelad|Neucha" rel="stylesheet">
    <script src='../js/jquery-min.js'></script>
    <script src="../js/jquery.json.min.js"></script>
    <title>Играть в КРЕСТИКИ-НОЛИКИ</title>
</head>

<body>
    <section class="need-rotate">
        <div>
            <span>Переверните девайс, чтобы играть.</span>
            <img src='../images/rotate.png'>
        </div>
    </section>
    <header>
        <div class='info'>
            <span>
                <b>Коэффициенты</b><br>О: 2.5<br>Х: 1.5<br>Ничья: 1.0
            </span>
        </div>
        <div class='welcome'>
            <span>
                Игра<br>"Крестики-Нолики"
            </span>
        </div>
        <div class='user-block'>
            <?php
                if (empty($_SESSION['login'])) {
                    echo "<b>Ваш игровой счет</b><br>Баланс: <span id='money'>0</span> драм";
                }
                else {
                    include ("../scripts/db.php");
                    $user = $_SESSION['login'];
                    $res = $db->query("SELECT * FROM users WHERE login='$user'");
                    $mydata = $res->fetch_assoc();
                    $_SESSION['balance'] = $mydata['balance'];
                    $avatar_id = $mydata['avatar_id'];
                    $avatar_res = $db->query("SELECT img FROM avatars WHERE id='$avatar_id'");
                    $avatar = $avatar_res->fetch_assoc();
                    $_SESSION['avatar'] = $avatar['img'];
                    echo "<div class='login-info'><div><img src='".$_SESSION['avatar']."'></div><div><b id='user-login'>".$_SESSION['login']."</b><br>Баланс: <span id='money'>".$_SESSION['balance']."</span> драм</div><div><a href='../scripts/exit.php' class='link-button' id='exit'>Выйти</a></div></div>";
                }
            ?>
        </div>
    </header>
    <main>
        <div id='new-game'>
            <span class='text'>Сыграем?</span>
            <button class='button' id='start-game'>Новая игра</button>
        </div>
        <div id='select'>
            <span class='text'>Выберете сторону:</span>
            <button id='select-x'>Х</button>
            <button id='select-o'>О</button>
        </div>
        <div id='betting'>
            <div class='text'>Ваша ставка: </div>
            <input class="link-button" type='number' min='100' step='50' value='100' class='button' id='bet'>
            <span class='text'> драм</span>
            <button id='make-bet'>Сделать ставку</button>
        </div>
        <div id="game">
            <span id='game-text'></span>
            <div id='table-box'>
                <table id='game-field'>
                    <tr>
                        <td id='td1'></td>
                        <td id='td2'></td>
                        <td id='td3'></td>
                    </tr>
                    <tr>
                        <td id='td4'></td>
                        <td id='td5'></td>
                        <td id='td6'></td>
                    </tr>
                    <tr>
                        <td id='td7'></td>
                        <td id='td8'></td>
                        <td id='td9'></td>
                    </tr>
                </table>
            </div>
        </div>
    </main>
    <script src='js/tic_tac_toe.js'></script>
</body>

</html>