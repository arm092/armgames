var table; // DOM Игровое поле
var gameText = ''; // Коментарий игры
var gamer; //за кого играет игрок
var comp; // за кого играет компьютер
var bet; // сумма ставки
var betInput; //input ставки
var ballance;
var bank = 0; // банк
var kayf; // Коэффицент
var games = 0; // Общее число сыгранных партий
var winGames = 0; // сколько игр выиграл компьютер
var loseGames = 0; // сколько игр проиграл компьютер
var win = true; // стратегия ИИ

var startGame;
var selectX;
var selectO;
var makeBet; // buttons

var newGame;
var select;
var betting;
var gameBlock; // blocks

function gameStart() {
    table = $('#game-field');
    gameText = $('#game-text');
    startGame = $('#start-game');
    startGame.on('click', NewGame);
    makeBet = $('#make-bet');
    makeBet.on('click', MakeBet);
    selectX = $('#select-x');
    selectX.on('click', SelectX);
    selectO = $('#select-o');
    selectO.on('click', SelectO);
    betting = $('#betting');
    newGame = $('#new-game');
    select = $('#select');
    gameBlock = $('#game');
}

function EmptyField() {
    $('#game-field').html("<tr><td id='td1'></td><td id='td2'></td><td id='td3'></td></tr><tr><td id='td4'></td><td id='td5'></td><td id='td6'></td></tr><tr><td id='td7'></td><td id='td8'></td><td id='td9'></td></tr>");

};

function NewGame() {
    newGame.hide();
    select.show();
}

function SelectX() {
    select.hide();
    comp = "O";
    gamer = "X";
    kayf = 1.5;
    $('#bet').attr('max', +$('#money').text());
    betting.show();
}

function SelectO() {
    select.hide();
    comp = "X";
    gamer = "O";
    kayf = 2.5;
    $('#bet').attr('max', +$('#money').text());
    betting.show();
}

function MakeBet() {
    $.getJSON('../scripts/get.php', { 'login': $('#user-login').text() }, function(res) {
        balance = +res.result;
        $('#money').text(balance);
    });
    betInput = $('#bet');
    bet = +betInput.val();
    ballance = +$('#money').text();
    if ((bet.isNaN) || (!((bet > 99) && (bet <= ballance)))) {
        betInput.val(100);
        bet = 0;
        $('#betting .text').text('Введите корректную сумму ставки');
        return;
    } else {
        $.getJSON('scripts/bet.php', { 'bet_sum': bet, 'select': gamer }, function(res) {
            balance = +res.result;
            $('#money').text(balance);
            games = +res.games;
            winGames = +res.wingames;
            win = res.win;
            bank = +res.bank_res;
        });
        $('#betting div.text').text('Ваша ставка:');
        betting.hide();
        $('.welcome').hide();
        gameBlock.show();
    }
    if (comp == 'X') {
        PaintField('Мы начинаем!');
        setTimeout(() => {
            CompTurn();
            for (let i = 1; i < 10; i++) {
                if ($('#td' + i).text() == '') {
                    $('#td' + i).on('click', UserTurn);
                } else {
                    $('#td' + i).off('click', UserTurn);
                }
            }
        }, 1500);
    } else {
        EmptyField();
        PaintField('Ваш ход!');
        for (let i = 1; i < 10; i++) {
            $('#td' + i).on('click', UserTurn);
        }
    }

}

function Search(elem) {
    let count = 0;
    for (let i = 1; i < 10; i++) {
        if ($('#td' + i).text() == elem) {
            count++;
        }
    }
    return count;
};

function end() {
    let check = CheckEnd();
    if (check) {
        $.getJSON('scripts/check.php', {
            'check': check
        }, function(res) {
            console.log(res.msg);

            balance = +res.result;
            $('#money').text(balance);
            bank = +res.bank_res;
            $('#game-text').text(res.msg);
        });
        setTimeout(function() {
            EmptyField();
            PaintField('');
            gameBlock.hide();
            newGame.show();
        }, 4000);
    } else return;
}

function UserTurn() {
    $(this).text(gamer);
    $(this).off('click', UserTurn);
    let check = CheckEnd();
    console.log(check);
    if (!check) {
        PaintField("Ваш ход сделан, теперь наша очередь.");
        for (let i = 1; i < 10; i++) {
            $('#td' + i).off();
        }
        setTimeout(() => {
            CompTurn();
            end();
            for (let i = 1; i < 10; i++) {
                if ($('#td' + i).text() == '') {
                    $('#td' + i).on('click', UserTurn);
                } else {
                    $('#td' + i).off('click', UserTurn);
                }
            }
        }, 1500);
    } else {
        end();
    }
};

function PaintField() {
    if (arguments.length > 0) {
        gameText.text(arguments[0]);
    };
};

function CompTurn() {
    PaintField('Ваш ход!');
    let x = 0;
    let y = 0;
    console.log(1);

    function mainRules(elem) {
        let x = 0;
        let y = 0;

        for (x = 0; x < table.find('tr').length; x++) {
            if ((table.find('tr').eq(x).find('td').eq(0).text() == table.find('tr').eq(x).find('td').eq(1).text()) && (table.find('tr').eq(x).find('td').eq(1).text() == elem) && (table.find('tr').eq(x).find('td').eq(2).text() == '')) {
                table.find('tr').eq(x).find('td').eq(2).text(comp);
                return true;
            } else if ((table.find('tr').eq(x).find('td').eq(0).text() == table.find('tr').eq(x).find('td').eq(2).text()) && (table.find('tr').eq(x).find('td').eq(2).text() == elem) && (table.find('tr').eq(x).find('td').eq(1).text() == '')) {
                table.find('tr').eq(x).find('td').eq(1).text(comp);
                return true;
            } else if ((table.find('tr').eq(x).find('td').eq(1).text() == table.find('tr').eq(x).find('td').eq(2).text()) && (table.find('tr').eq(x).find('td').eq(2).text() == elem) && (table.find('tr').eq(x).find('td').eq(0).text() == '')) {
                table.find('tr').eq(x).find('td').eq(0).text(comp);
                return true;
            } else if ((table.find('tr').eq(0).find('td').eq(x).text() == table.find('tr').eq(1).find('td').eq(x).text()) && (table.find('tr').eq(1).find('td').eq(x).text() == elem) && (table.find('tr').eq(2).find('td').eq(x).text() == '')) {
                table.find('tr').eq(2).find('td').eq(x).text(comp);
                return true;
            } else if ((table.find('tr').eq(0).find('td').eq(x).text() == table.find('tr').eq(2).find('td').eq(x).text()) && (table.find('tr').eq(2).find('td').eq(x).text() == elem) && (table.find('tr').eq(1).find('td').eq(x).text() == '')) {
                table.find('tr').eq(1).find('td').eq(x).text(comp);
                return true;
            } else if ((table.find('tr').eq(1).find('td').eq(x).text() == table.find('tr').eq(2).find('td').eq(x).text()) && (table.find('tr').eq(2).find('td').eq(x).text() == elem) && (table.find('tr').eq(0).find('td').eq(x).text() == '')) {
                table.find('tr').eq(0).find('td').eq(x).text(comp);
                return true;
            }
        } //главные правила по вертикали и горизонтали
        if (($('#td1').text() == $('#td5').text()) && ($('#td5').text() == elem) && ($('#td9').text() == '')) {
            $('#td9').text(comp);
            return true;
        } else if (($('#td5').text() == $('#td9').text()) && ($('#td5').text() == elem) && ($('#td1').text() == '')) {
            $('#td1').text(comp);
            return true;
        } else if (($('#td1').text() == $('#td9').text()) && ($('#td9').text() == elem) && ($('#td5').text() == '')) {
            $('#td5').text(comp);
            return true;
        } else if (($('#td7').text() == $('#td5').text()) && ($('#td5').text() == elem) && ($('#td3').text() == '')) {
            $('#td3').text(comp);
            return true;
        } else if (($('#td7').text() == $('#td3').text()) && ($('#td3').text() == elem) && ($('#td5').text() == '')) {
            $('#td5').text(comp);
            return true;
        } else if (($('#td5').text() == $('#td3').text()) && ($('#td5').text() == elem) && ($('#td7').text() == '')) {
            $('#td7').text(comp);
            return true;
        } else return false; //главные правила по диаганали
    }
    if (mainRules(comp)) {
        return;
    } else if (mainRules(gamer)) {
        return;
    }

    if (!(win)) {
        do {
            x = Math.floor(Math.random() * 3);
            y = Math.floor(Math.random() * 3);
            if (table.find('tr').eq(x).find('td').eq(y).text() == '') {
                table.find('tr').eq(x).find('td').eq(y).text(comp);
                return;
            } else {
                x = 0;
            }
        } while (x == 0)
    } //случайный ход
    if ($('#td5').text() == '') {
        $('#td5').text(comp);
        return;
    } //ход в центр
    if (comp == 'X') {
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                if (table.find('tr').eq(i).find('td').eq(j).text() == gamer) {
                    x = j;
                    y = i;
                    if (x == 1) {
                        x++;
                    }
                    if (y == 1) {
                        y++;
                    }
                    if (table.find('tr').eq(2 - y).find('td').eq(2 - x).text() == '') {
                        table.find('tr').eq(2 - y).find('td').eq(2 - x).text(comp);
                        return;
                    }
                }
            }
        }
    } //ход в противоположенный угол	
    x = 0;
    y = 0;
    while (x < 3) {
        while (y < 3) {
            if ((table.find('tr').eq(x).find('td').eq(y).text() == gamer) && (table.find('tr').eq(2 - x).find('td').eq(y).text() == '')) {
                if ((table.find('tr').eq(2 - x).find('td').eq(2 - y).text() == gamer) && (table.find('tr').eq(2 - x).find('td').eq(1).text() == '')) {
                    table.find('tr').eq(2 - x).find('td').eq(1).text(comp);
                    return;
                }
                table.find('tr').eq(2 - x).find('td').eq(y).text(comp);
                return;
            }
            y += 2
        }
        x += 2;
    } //анти-детский мат
    x = 0;
    y = 0;
    if ((table.find('tr').eq(0).find('td').eq(1).text() == table.find('tr').eq(1).find('td').eq(0).text()) && (table.find('tr').eq(1).find('td').eq(0).text() == gamer) && ($('#td1').text() == '')) {
        $('#td1').text(comp);
        return;
    } else if ((table.find('tr').eq(0).find('td').eq(1).text() == table.find('tr').eq(1).find('td').eq(2).text()) && (table.find('tr').eq(1).find('td').eq(2).text() == gamer) && ($('#td3').text() == '')) {
        $('#td3').text(comp);
        return;
    } else if ((table.find('tr').eq(2).find('td').eq(1).text() == table.find('tr').eq(1).find('td').eq(0).text()) && (table.find('tr').eq(1).find('td').eq(0).text() == gamer) && ($('#td7').text() == '')) {
        $('#td7').text(comp);
        return;
    } else if ((table.find('tr').eq(2).find('td').eq(1).text() == table.find('tr').eq(1).find('td').eq(2).text()) && (table.find('tr').eq(1).find('td').eq(2).text() == gamer) && ($('#td9').text() == '')) {
        $('#td9').text(comp);
        return;
    } //ход в угол спец.
    while (x < 3) {
        while (y < 3) {
            if (table.find('tr').eq(x).find('td').eq(y).text() == '') {
                table.find('tr').eq(x).find('td').eq(y).text(comp);
                return;
            }
            y += 2
        }
        x += 2;
    } //ход в угол
    for (x = 0; x < 3; x++) {
        for (y = 1; y < 3; y++) {
            if (table.find('tr').eq(x).find('td').eq(y).text() == '') {
                table.find('tr').eq(x).find('td').eq(y).text(comp);
                return;
            } else if (table.find('tr').eq(y).find('td').eq(x).text() == '') {
                table.find('tr').eq(y).find('td').eq(x).text(comp);
                return;
            };
        };
    }; //обычный ход

};

function CheckEnd() {

    for (let x = 0; x < 3; x++) {
        if ((table.find('tr').eq(x).find('td').eq(0).text() == table.find('tr').eq(x).find('td').eq(1).text()) && (table.find('tr').eq(x).find('td').eq(1).text() == table.find('tr').eq(x).find('td').eq(2).text()) && (table.find('tr').eq(x).find('td').eq(1).text() != '')) {
            table.find('tr').eq(x).find('td').eq(0).css('background-color', '#bf1e2d').css('color', 'white');
            table.find('tr').eq(x).find('td').eq(1).css('background-color', '#bf1e2d').css('color', 'white');
            table.find('tr').eq(x).find('td').eq(2).css('background-color', '#bf1e2d').css('color', 'white');
            return table.find('tr').eq(x).find('td').eq(1).text();
        } else if ((table.find('tr').eq(0).find('td').eq(x).text() == table.find('tr').eq(1).find('td').eq(x).text()) && (table.find('tr').eq(2).find('td').eq(x).text() == table.find('tr').eq(1).find('td').eq(x).text()) && (table.find('tr').eq(2).find('td').eq(x).text() != '')) {
            table.find('tr').eq(0).find('td').eq(x).css('background-color', '#bf1e2d').css('color', 'white');
            table.find('tr').eq(1).find('td').eq(x).css('background-color', '#bf1e2d').css('color', 'white');
            table.find('tr').eq(2).find('td').eq(x).css('background-color', '#bf1e2d').css('color', 'white');
            return table.find('tr').eq(1).find('td').eq(x).text();
        }
    }
    if (($('#td1').text() == $('#td5').text()) && ($('#td9').text() == $('#td5').text()) && ($('#td9').text() != '')) {
        table.find('tr').eq(0).find('td').eq(0).css('background-color', '#bf1e2d').css('color', 'white');
        table.find('tr').eq(1).find('td').eq(1).css('background-color', '#bf1e2d').css('color', 'white');
        table.find('tr').eq(2).find('td').eq(2).css('background-color', '#bf1e2d').css('color', 'white');
        return $('#td5').text();
    } else if (($('#td7').text() == $('#td5').text()) && ($('#td3').text() == $('#td5').text()) && ($('#td3').text() != '')) {
        table.find('tr').eq(2).find('td').eq(0).css('background-color', '#bf1e2d').css('color', 'white');
        table.find('tr').eq(1).find('td').eq(1).css('background-color', '#bf1e2d').css('color', 'white');
        table.find('tr').eq(0).find('td').eq(2).css('background-color', '#bf1e2d').css('color', 'white');
        return $('#td5').text();
    }
    if (Search('') == 0) {
        return "draw";
    } else {
        return false;
    }
}

$(document).ready(gameStart);